package com.arsentyr.asteroids;

import opengl.GLSurfaceView;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

import com.arsentyr.utility.Logger;

public class GameSurfaceView extends GLSurfaceView {

    private ProxyNative proxyNative = new ProxyNative();
    
    private int width = 0;
    private int height = 0;
	
	public GameSurfaceView(Context context) {
        super(context);
        
        initialize();
    }
	
    public GameSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        
        initialize();
    }
	
	protected void initialize() {
        if (Logger.LOGGABLE_DEBUG) {
            setDebugFlags(DEBUG_CHECK_GL_ERROR | DEBUG_LOG_GL_CALLS);
        }		

        setFocusableInTouchMode(true);
		
        setEGLContextClientVersion(2);
        setEGLConfigChooser(8, 8, 8, 8, 0, 0);
        setPreserveEGLContextOnPause(true);

        setRenderer(proxyNative);
	}

    @Override
    public void onResume() {
        super.onResume();
        touchCancel(pan.id);
        touchCancel(second.id);
        touchCancel(first.id);
        queueEvent(new Runnable() {
            public void run() {
                proxyNative.handleResume();
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        queueEvent(new Runnable() {
            public void run() {
                proxyNative.handlePause();
            }
        });
    }

    @Override
    public void onWindowFocusChanged(final boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        queueEvent(new Runnable() {
            public void run() {
                proxyNative.handleWindowFocusChanged(hasWindowFocus);
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            queueEvent(new Runnable() {
                public void run() {
                    proxyNative.handleFinish();
                    Activity activity = (Activity) getContext();
                    activity.finish();
                }
            });
        }
        return true;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return true;
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        switch (event.getActionMasked()) {
        case MotionEvent.ACTION_DOWN:
            touchDown(event.getPointerId(0), event.getX(), event.getY());
            break;
       
        case MotionEvent.ACTION_POINTER_DOWN: {
                final int index = event.getActionIndex();
                touchDown(event.getPointerId(index), event.getX(index), event.getY(index));
            }
            break;

        case MotionEvent.ACTION_UP:
            touchUp(event.getPointerId(0), event.getX(), event.getY());
            break;

        case MotionEvent.ACTION_POINTER_UP: {
                final int index = event.getActionIndex();
                touchUp(event.getPointerId(index), event.getX(index), event.getY(index));
            }
            break; 

        case MotionEvent.ACTION_MOVE:
            for (int index = 0; index < event.getPointerCount(); ++index) {
                touchMove(event.getPointerId(index), event.getX(index), event.getY(index));
            } 
            break;

        case MotionEvent.ACTION_CANCEL:
            for (int index = 0; index < event.getPointerCount(); ++index) {
                touchCancel(event.getPointerId(index));
            }
            break;
        }
        return true;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        super.surfaceChanged(holder, format, w, h);
        
        width = w;
        height = h;
        
        touchCancel(pan.id);
        touchCancel(second.id);
        touchCancel(first.id);
        
        // FIXME: take into account the DPI of the scaling
        deltaTap = Math.min(w, h) * 0.075f;
    }

    private static class Pointer {
        public static final int UNDEFINED_ID = -1;
        public int id = UNDEFINED_ID;
        public float x = 0;
        public float y = 0;
    }

    private float deltaTap = 1;

    private Pointer first = new Pointer();
    private Pointer second = new Pointer();
    private Pointer pan = new Pointer();

    private void touchDown(int id, float x, float y) {
        touchCancel(id);
        if (first.id == Pointer.UNDEFINED_ID) {
            first.id = id;
            first.x = x;
            first.y = y;
        }
        else if ((pan.id == Pointer.UNDEFINED_ID) && (second.id == Pointer.UNDEFINED_ID)) {
            second.id = id;
            second.x = x;
            second.y = y;
        }
    }

    private void touchUp(int id, float x, float y) {
        if (first.id == id) {
            if (isTap(x - first.x, y - first.y)) {
                tap(x, y);
            }
            touchCancel(first.id);
        }
        else if (second.id == id) {
            if (isTap(x - second.x, y - second.y)) {
                tap(x, y);
            }
            touchCancel(second.id);
        }
        else if (pan.id == id) {
            final float dx = x - pan.x;
            final float dy = y - pan.y;
            if (!isTap(dx, dy)) {
                pan(dx, dy);
            }
            touchCancel(pan.id);
        }
    }

    private void touchMove(int id, float x, float y) { 
        if (first.id == id) {
            final float dx = x - first.x;
            final float dy = y - first.y;
            if (!isTap(dx, dy)) {
                if (pan.id == Pointer.UNDEFINED_ID) {
                    pan.id = id;
                    pan.x = first.x + dx;
                    pan.y = first.y + dy;
                    pan(dx, dy);
                }
                touchCancel(first.id);
            }
        }
        else if (second.id == id) {
            final float dx = x - second.x;
            final float dy = y - second.y;
            if (!isTap(dx, dy)) {
                if (pan.id == Pointer.UNDEFINED_ID) {
                    pan.id = id;
                    pan.x = second.x + dx;
                    pan.y = second.y + dy;
                    pan(dx, dy);
                }
                touchCancel(second.id);
            }
        }
        else if (pan.id == id) {
            final float dx = x - pan.x;
            final float dy = y - pan.y;
            if (!isTap(dx, dy)) {
                pan.x = x;
                pan.y = y;
                pan(dx, dy);
            }
        }
    }

    private void touchCancel(int id) {
        if (first.id == id) {
            first.id = second.id;
            if (second.id != Pointer.UNDEFINED_ID) {
                first.x = second.x;
                first.y = second.y;
                second.id = Pointer.UNDEFINED_ID;
            } 
        }
        else if (second.id == id) {
            second.id = Pointer.UNDEFINED_ID;
        }
        else if (pan.id == id) {
            pan.id = Pointer.UNDEFINED_ID;
        }
    }

    private boolean isTap(float dx, float dy) {
        return (Math.abs(dx) <= deltaTap) && (Math.abs(dy) <= deltaTap);
    }

    private void tap(final float x, final float y) {
        if (x >= 0 && x <= width && y >= 0 && y <= height) {
            queueEvent(new Runnable() {
                public void run() {
                    proxyNative.handleTap(x, y);
                }
            });
        }
    }

    private void pan(final float dx, final float dy) {
        queueEvent(new Runnable() {
            public void run() {
                proxyNative.handlePan(dx, dy);
            }
        });
    } 

    private static class ProxyNative implements GLSurfaceView.Renderer {

        private static final Logger log = new Logger(ProxyNative.class.getSimpleName());

        private boolean hasResume = false;
        private boolean hasFocus = false;
        private boolean isSurfaceCreated = false;
        private boolean isSurfaceChanged = false;
        private boolean isResumed = false;

        public void onSurfaceCreated(GL10 gl, EGLConfig config) {
            log.debug("onSurfaceCreated");

            if (isSurfaceCreated) {
                checkPausing();
                nativeResetGraphicsResources();
            } else {
                isSurfaceCreated = true;
                nativeInitialize();
            }

            isSurfaceChanged = false;
        }
                
        public void onSurfaceChanged(GL10 gl, int width, int height) {
            log.debug("onSurfaceChanged: width=%d, height=%d", width, height);

            isSurfaceChanged = (width > 0 && height > 0 && width >= height);
            if (isSurfaceChanged) {
                nativeResize(width, height);
            }
            checkResuming();
        }
        
        public void onDrawFrame(GL10 gl) {
            if (isResumed) {
                nativeDrawFrame();
            }
        }

        public void handleWindowFocusChanged(boolean hasWindowFocus) {
            log.debug("handleWindowFocusChanged: hasWindowFocus=%b", hasWindowFocus);
            
            hasFocus = hasWindowFocus;
            if (hasFocus) {
                checkResuming();
            }
            else {
                checkPausing();
            }
        }

        public void handleResume() {
            log.debug("handleResume");

            hasResume = true;
            checkResuming();
        }

        public void handlePause() {
            log.debug("handlePause");

            hasResume = false;
            checkPausing();
        }

        public void handleFinish() {
            log.debug("handleFinish");

            isSurfaceCreated = false;
            isSurfaceChanged = false;
            checkPausing();
        }

        public void handleTap(float x, float y) {
            if (isResumed) {
                nativeTap(x, y);
            }
        }

        public void handlePan(float dx, float dy) {
            if (isResumed) {
                nativePan(dx, dy);
            }
        }

        private void checkResuming() {
            if (!isResumed
                    && isSurfaceChanged && hasResume && hasFocus) {
                isResumed = true;
                nativeResume();
            }
        }

        private void checkPausing() {
            if (isResumed) {
                isResumed = false;
                nativePause();
            }
        }

        private static native void nativeInitialize();
        private static native void nativeResetGraphicsResources();
        private static native void nativeResize(int width, int height);
        private static native void nativeResume();
        private static native void nativeDrawFrame();
        private static native void nativePause();
        private static native void nativeTap(float x, float y);
        private static native void nativePan(float dx, float dy);
    }

    static {
        System.loadLibrary("asteroids");
    }
}
