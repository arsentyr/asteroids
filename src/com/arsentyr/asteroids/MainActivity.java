package com.arsentyr.asteroids;

import android.app.Activity;
import android.os.Bundle;
import android.os.Process;

public class MainActivity extends Activity {

    private GameSurfaceView view;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        view = (GameSurfaceView) findViewById(R.id.game_surface_view);
    }

    @Override
    protected void onResume() {
        super.onResume();
        view.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        view.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        int pid = Process.myPid();
        Process.killProcess(pid);
    }    
}
