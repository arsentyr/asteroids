package com.arsentyr.utility;

import android.util.Log;

public class Logger {

	public static final boolean LOGGABLE_VERBOSE = false;
	public static final boolean LOGGABLE_DEBUG = false;
	public static final boolean LOGGABLE_INFO = false;
	public static final boolean LOGGABLE_WARNING = false;
	public static final boolean LOGGABLE_ERROR = true;
	
	public final String tag;
	
	public Logger(String tag) {
		this.tag = tag;
	}
	
	public void verbose(String msg) {
		if (LOGGABLE_VERBOSE) {
			Log.v(tag, msg);
		}
	}
	
	public void verbose(String format, Object... args) {
		if (LOGGABLE_VERBOSE) {
			Log.v(tag, String.format(format, args));
		}
	}
	
	public void debug(String msg) {
		if (LOGGABLE_DEBUG) {
			Log.d(tag, msg);
		}
	}
		
	public void debug(String format, Object... args) {
		if (LOGGABLE_DEBUG) {
			Log.d(tag, String.format(format, args));
		}
	}
	
	public void info(String msg) {
		if (LOGGABLE_INFO) {
			Log.i(tag, msg);
		}
	}
	
	public void info(String format, Object... args) {
		if (LOGGABLE_INFO) {
			Log.i(tag, String.format(format, args));
		}
	}
	
	public void warning(String msg) {
		if (LOGGABLE_WARNING) {
			Log.w(tag, msg);
		}
	}
	
	public void warning(String format, Object... args) {
		if (LOGGABLE_WARNING) {
			Log.w(tag, String.format(format, args));
		}
	}

	public void error(String msg) {
		if (LOGGABLE_ERROR) {
			Log.e(tag, msg);
		}
	}
	
	public void error(String format, Object... args) {
		if (LOGGABLE_ERROR) {
			Log.e(tag, String.format(format, args));
		}
	}
	
}
