APP_ABI := all
APP_PLATFORM := android-9
APP_OPTIM := debug
APP_MODULES := asteroids
APP_CPPFLAGS := -std=c++11 -Wall
APP_STL := gnustl_static
NDK_TOOLCHAIN_VERSION := 4.7
