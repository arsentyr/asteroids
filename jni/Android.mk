LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

MY_LOG_TAG := \"Asteroids\"

LOCAL_MODULE := asteroids
LOCAL_SRC_FILES += $(subst $(LOCAL_PATH),,$(wildcard $(LOCAL_PATH)/src/*.cpp $(LOCAL_PATH)/src/Core/*.cpp $(LOCAL_PATH)/src/Core/Math/*.cpp $(LOCAL_PATH)/src/Core/Graphics/*.cpp))
LOCAL_CFLAGS += -DLOG_TAG=$(MY_LOG_TAG)
LOCAL_LDLIBS := -llog -lGLESv2

include $(BUILD_SHARED_LIBRARY)
