#include <jni.h>

#include "Application.h"

using App = Asteroids::Application;

#define JNI_VERSION JNI_VERSION_1_6
#define ANDROID_NATIVE_CLASS_NAME "com/arsentyr/asteroids/GameSurfaceView$ProxyNative"

static void Initialize(JNIEnv*, jclass) {
	App::Instance().Initialize();
}

static void ResetGraphicsResources(JNIEnv*, jclass) {
	App::Instance().ResetGraphicsResources();
}

static void Resize(JNIEnv*, jclass, jint width, jint height) {
	App::Instance().Resize(width, height);
}

static void Resume(JNIEnv*, jclass) {
	App::Instance().Resume();
}

static void DrawFrame(JNIEnv*, jclass) {
	App::Instance().DrawFrame();
}

static void Pause(JNIEnv*, jclass) {
	App::Instance().Pause();
}

static void Tap(JNIEnv*, jclass, jfloat x, jfloat y) {
	App::Instance().Tap(x, y);
}

static void Pan(JNIEnv*, jclass, jfloat dx, jfloat dy) {
	App::Instance().Pan(dx, dy);
}

extern "C"
jint JNI_OnLoad(JavaVM* vm, void* reserved) {
	JNIEnv* env = nullptr;
	if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION) != JNI_OK) {
		return JNI_ERR;
	}

	const JNINativeMethod methods[] = {
		{
			"nativeInitialize",
			"()V",
			(void*)Initialize
		},
		{
			"nativeResetGraphicsResources",
			"()V",
			(void*)ResetGraphicsResources
		},
		{
			"nativeResize",
			"(II)V",
			(void*)Resize
		},
		{
			"nativeResume",
			"()V",
			(void*)Resume
		},
		{
			"nativeDrawFrame",
			"()V",
			(void*)DrawFrame
		},
		{
			"nativePause",
			"()V",
			(void*)Pause
		},
		{
			"nativeTap",
			"(FF)V",
			(void*)Tap
		},
		{
			"nativePan",
			"(FF)V",
			(void*)Pan
		}
	};
	if (env->RegisterNatives(env->FindClass(ANDROID_NATIVE_CLASS_NAME), methods, sizeof(methods) / sizeof(JNINativeMethod)) != JNI_OK) {
		return JNI_ERR;
	}
	return JNI_VERSION;
}