#ifndef ASTEROIDS_ASTEROID_H
#define ASTEROIDS_ASTEROID_H

#include <vector>
#include "Core/Core.h"
#include "Pool.h"

namespace Asteroids {

class Asteroid : public Poolable {
public:
	Asteroid();

	void Reset(float radius, const Vector2& position, const Vector2& velocity);

	void Scale(float scaleRadius, const Vector2& scaleAxis);

	bool IsFinished() const;

	void Draw() const;

	void Update(float deltaTime);

private:
	enum class State {
		Created,
		Starting,
		Started,
		Finishing,
		Finished
	};

public:
	class Fragment {
	friend class Asteroid;
	public:
		float GetRadius() const;

		Vector2 GetPosition() const;

		Vector2 GetVelocity() const;

		float GetAngle() const;

		bool Hit(const Vector2& startPosition, const Vector2& endPosition, float radius);

		void Clip(int width, int height);

		bool IsDrawable() const;

		bool Update(float deltaTime);

	protected:	
		Fragment(Asteroid& asteroid);

		void Reset(float angle, float radius, const Vector2& position, const Vector2& velocity);

		void Start();

		void Resume();

		void Scale(float scaleRadius, const Vector2& scaleAxis);

		void SetAngle(float angle);

		void SetRadius(float radius, float duration = 0);

		void SetPosition(const Vector2& position);

		void SetVelocity(const Vector2& velocity);

		void Draw() const;

		static bool CircleCircleSweepTest(const Vector2& firstStartPosition,
			const Vector2& firstEndPosition, float firstRadius,
			const Vector2& secondStartPosition, const Vector2& secondEndPosition,
			float secondRadius);

	private:
		Asteroid& _asteroid;
		
		State _state = State::Created;
		Tweenf _angle{0, 360, TweenEasing::QuadInOut, Random::NextFloat(3, 5), TweenMode::LoopPingPong};
		Tweenf _radius{0.5, 0.5, TweenEasing::CubicOut, 1, TweenMode::Forward, false};
		Vector2 _prevPosition{0, 0};
		Vector2 _currPosition{0, 0};
		Vector2 _velocity{0, 0};
	};

protected:
	void OnFragmentHit();

	void OnFragmentFinished();

private:
	typedef std::vector<Fragment> Fragments;

	enum {
		kMinNumberOfFragments = 2,
		kMaxNumberOfFragments = 4
	};

	static constexpr float kDurationFinishing = 0.5; 

	enum class Shape {
		Triangle,
		Quadrate,
		Rhomb
	};

private:
	State _state = State::Created;
	Shape _shape = Shape::Triangle;
	float _radius = 0.5;
	VertexBuffer _vertices;

	std::size_t _numFragments = static_cast<std::size_t>(Random::NextInt(kMinNumberOfFragments, kMaxNumberOfFragments));
	Fragments _fragments{_numFragments, Fragment{*this}};

public:
	class ForwardIterator {
	friend class Asteroid;
	public:
		ForwardIterator() = delete;
		ForwardIterator(const ForwardIterator&) = default;
		ForwardIterator(ForwardIterator&&) = default;

		ForwardIterator& operator=(const ForwardIterator&) = delete;
		ForwardIterator& operator=(ForwardIterator&&) = delete;

		bool operator!=(const ForwardIterator& iterator) const;

		ForwardIterator& operator++();

		Fragment& operator*();

	private:
		ForwardIterator(Fragments& fragments, std::size_t position);

	private:
		Fragments& _fragments;
		std::size_t _position;
	};

public:
	ForwardIterator begin();

	const ForwardIterator end();
};

} /* namespace Asteroids */

#endif /* ASTEROIDS_ASTEROID_H */