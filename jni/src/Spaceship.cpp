#include "Spaceship.h"

namespace Asteroids {

void Spaceship::Reset(float radius, const Vector2& position, const Vector2& velocity) {
	SetRadius(radius);
	SetPosition(position);
	SetVelocity(velocity);
	Stop();
}

void Spaceship::Scale(float scaleRadius, const Vector2& scaleAxis) {
	Assert(scaleRadius > 0);

	_radius *= scaleRadius;
	_position *= scaleAxis;

	const auto velocity = scaleAxis * GetVelocity();
	SetVelocity(velocity);
}

void Spaceship::SetRadius(float radius) {
	Assert(radius > 0);
	_radius = radius;
}

float Spaceship::GetRadius() const {
	return _radius;
}

float Spaceship::GetAngle() const {
	return _angle;
}

void Spaceship::SetPosition(const Vector2& position) {
	_position = position;
}

Vector2 Spaceship::GetPosition() const {
	return _position;
}

void Spaceship::SetVelocity(const Vector2& velocity, float duration) {
	Assert(duration >= 0);

	if (duration > 0) {
		_velocity.SetInitial(_velocity.Value());
		_velocity.SetEnd(velocity);
		_velocity.SetTime(0);
		_velocity.SetDuration(duration);
	}
	else {
		_velocity.SetInitial(velocity);
		_velocity.SetEnd(velocity);
		_velocity.SetTime(_velocity.GetDuration());
	}
	_velocity.Resume();
	Update(0);
}

Vector2 Spaceship::GetVelocity() const {
	return _velocity.Value();
}

void Spaceship::Stop() {
	SetVelocity(Vector2{0, 0});
	_velocity.Pause();
}

void Spaceship::Clip(int width, int height) {
	//TODO: ease stopping
	if ((_position.x - _radius) < 0) {
		_position.x = _radius;
		Stop();
	}
	else if ((_position.x + _radius) > width) {
		_position.x = width - _radius;
		Stop();
	}

	if ((_position.y - _radius) < 0) {
		_position.y = _radius;
		Stop();
	}
	else if ((_position.y + _radius) > height) {
		_position.y = height - _radius;
		Stop();
	}
}

void Spaceship::Draw() const {
	static const VertexBuffer vertices = {
		Vertex{ 1,    0,    0, Color(32, 170, 214, 92)},
		Vertex{-0.5,  0.75, 0, Color(32, 170, 214, 92)},
		Vertex{-1,    0.5,  0, Color(32, 170, 214, 92)},
		Vertex{-0.5,  0,    0, Color(32, 170, 214, 92)},
		Vertex{-1,   -0.5,  0, Color(32, 170, 214, 92)},
		Vertex{-0.5, -0.75, 0, Color(32, 170, 214, 92)}
	};

	Core::graphics.PushMatrix();
	Core::graphics.Translate(_position);
	Core::graphics.Rotate(_angle);
	Core::graphics.Scale(_radius);
	Core::graphics.SetBlendMode(BlendMode::Add);
	Core::graphics.SetLineWidth(4);
	Core::graphics.Draw(vertices, DrawMode::LineLoop);
	Core::graphics.SetLineWidth(2);
	Core::graphics.Draw(vertices, DrawMode::LineLoop);
	Core::graphics.SetLineWidth(1);
	Core::graphics.Draw(vertices, DrawMode::LineLoop);
	Core::graphics.Draw(vertices, DrawMode::TriangleFan);
	Core::graphics.SetBlendMode(BlendMode::Alpha);
	Core::graphics.PopMatrix();
}

void Spaceship::Update(float deltaTime) {
	const auto isChanged = _velocity.Update(deltaTime);
	const auto velocity = _velocity.Value();

	_position += velocity * deltaTime;
	
	if (isChanged) {
		const auto length = velocity.Length();
		if (length > 0) {
			_angle = RadToDeg(Acos(velocity.x / length));
			if (velocity.y < 0) {
				_angle = 360 - _angle;
			}
		}
	}
}

} /* namespace Asteroids */