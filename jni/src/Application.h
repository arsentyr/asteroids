#ifndef ASTEROIDS_APPLICATION_H
#define ASTEROIDS_APPLICATION_H

#include "Space.h"

namespace Asteroids {

class Application {
public:
	Application(const Application&) = delete;
	Application(Application&&) = delete;

	Application& operator=(const Application&) = delete;
	Application& operator=(Application&&) = delete;

	~Application() = default;

	static Application& Instance();

	void Initialize();

	void ResetGraphicsResources();

	void Resize(int widht, int height);

	void Resume();

	void DrawFrame();

	void Pause();

	void Tap(float x, float y);

	void Pan(float dx, float dy);	

	void Develop(bool isDevelop);

	bool IsDevelop() const;

private:
	Application() = default;

	static void DrawFps(float x, float y, float fontSize, float deltaTime);

private:
	enum class State {
		Default,
		Initialized,
		Resumed,
		Paused
	};

private:
	bool _isDevelop = false;
	State _state = State::Default;
	Space _space;
};

} /* namespace Asteroids */

#endif /* ASTEROIDS_APPLICATION_H */