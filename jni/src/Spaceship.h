#ifndef ASTEROIDS_SPACESHIP_H
#define ASTEROIDS_SPACESHIP_H

#include "Core/Core.h"
#include "Shot.h"

namespace Asteroids {

class Spaceship {
public:
	void Reset(float radius, const Vector2& position, const Vector2& velocity);

	void Scale(float scaleRadius, const Vector2& scaleAxis);

	void SetRadius(float radius);
	float GetRadius() const;

	float GetAngle() const;

	void SetPosition(const Vector2& position);
	Vector2 GetPosition() const;
 
	void SetVelocity(const Vector2& velocity, float duration = 0);
	Vector2 GetVelocity() const;

	void Stop();

	void Clip(int width, int height);

	void Draw() const;

	void Update(float deltaTime);

private:
	float _angle = 0;
	float _radius = 0.05;
	Vector2 _position{0.5, 0.5};
	Tweenv _velocity{Vector2{0, 0}, Vector2{0, 0}, TweenEasing::Linear, 1, TweenMode::Forward, false};
};

}

#endif /* ASTEROIDS_SPACESHIP_H */