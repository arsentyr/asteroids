#ifndef ASTEROIDS_SPACE_H
#define ASTEROIDS_SPACE_H

#include "Core/Core.h"
#include "Asteroid.h"
#include "Pool.h"
#include "Spaceship.h"
#include "Shot.h"

namespace Asteroids {

class Space {
public:
	Space();

	void Reset();

	void Resize(int width, int height);
	
	int GetWidth() const;
	
	int GetHeight() const;

	void Draw() const;

	void Update(float deltaTime);

	void Tap(float x, float y);

	void Pan(float dx, float dy);

private:
	void DrawDevelop() const;

	void GenerateAsteroid();
	void DeleteAsteroid(const Asteroid& asteroid);

private:
	static constexpr int kMinNumberOfAsteroids = 3;
	static constexpr float kMinDelayAsteroid = 1;
	static constexpr float kMaxDelayAsteroid = 3;
	static constexpr float kMinDurationTweenVelocitySpacehip = 0.5;
	
private:
	int _width = 1;
	int _height = 1;

	float _minRadiusAsteroid = 0.045;
	float _maxRadiusAsteroid = 0.075;
	float _minVelocityAsteroid = 0.05;
	float _maxVelocityAsteroid = 0.15;

	float _minRadiusSpaceship = 0.05;
	float _maxVelocitySpaceship = _maxVelocityAsteroid * 1.75;

	float _maxRadiusShot = _minRadiusAsteroid * 0.4;
	float _minVelocityShot = _maxVelocitySpaceship * 1.5;

	Pool<Asteroid> _asteroids{50};
	int _numAsteroids = 0;
	float _delayAsteroid = kMinDelayAsteroid;
	
	Pool<Shot> _shots{50};
	Spaceship _spaceship;
};

} /* namespace Asteroids */

#endif /* ASTEROIDS_SPACE_H */