#ifndef ASTEROIDS_SHOT_H
#define ASTEROIDS_SHOT_H

#include "Core/Core.h"
#include "Pool.h"

namespace Asteroids {

class Shot : public Poolable {
public:
	void Reset(float radius, const Vector2& position, const Vector2& velocity);

	void Scale(float scaleRadius, const Vector2& scaleAxis);

	float GetRadius() const;

	Vector2 GetPrevPosition() const;
	Vector2 GetCurrPosition() const;

	Vector2 GetVelocity() const;

	void Clip(float width, float height);

	bool IsFinished() const;

	void Draw() const;

	void Update(float deltaTime);

private:
	static constexpr float kLifespan = 1.55;
	
private:
	float _angle = 0;
	float _radius = 0.5;
	Vector2 _prevPosition{0, 0};
	Vector2 _currPosition{0, 0};
	Vector2 _velocity{0, 0};

	float _time = 0;
	float _alpha = 255;
};

} /* namespace Asteroids */

#endif /* ASTEROIDS_SHOT_H */