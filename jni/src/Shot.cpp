#include "Shot.h"

namespace Asteroids {

void Shot::Reset(float radius, const Vector2& position, const Vector2& velocity) {
	Assert(radius > 0);

	_time = 0;
	_radius = radius;
	_prevPosition = position;
	_currPosition = position;
	_velocity = velocity;

	const auto length = _velocity.Length();
	Assert(length > 0);
	if (length > 0) {
		_angle = RadToDeg(Acos(velocity.x / length));
		if (velocity.y < 0) {
			_angle = 360 - _angle;
		}
	}
	else {
		_angle = 0;
	}
} 

void Shot::Scale(float scaleRadius, const Vector2& scaleAxis) {
	Assert(scaleRadius > 0);

	_radius *= scaleRadius;
	_prevPosition *= scaleAxis;
	_currPosition *= scaleAxis;
	_velocity *= scaleAxis;
}

float Shot::GetRadius() const {
	return _radius;
}

Vector2 Shot::GetPrevPosition() const {
	return _prevPosition;
}

Vector2 Shot::GetCurrPosition() const {
	return _currPosition;
}

Vector2 Shot::GetVelocity() const {
	return _velocity;
}

void Shot::Clip(float width, float height) {
	Assert(width > 0 && height > 0);

	if ((_currPosition.x - _radius) <= 0) {
		_currPosition.x = _radius;
		_velocity.x = -_velocity.x;
		_angle = 180 - _angle;
	}
	else if ((_currPosition.x + _radius) >= width) {
		_currPosition.x = width - _radius;
		_velocity.x = -_velocity.x;
		_angle = 180 - _angle;
	}

	if ((_currPosition.y - _radius) <= 0) {
		_currPosition.y = _radius;
		_velocity.y = -_velocity.y;
		_angle = 360 - _angle;
	}
	else if ((_currPosition.y + _radius) >= height) {
		_currPosition.y = height - _radius;
		_velocity.y = -_velocity.y;
		_angle = 360 - _angle;
	}
}

bool Shot::IsFinished() const {
	return _time >= kLifespan;
}

void Shot::Draw() const {
	Assert(_time <= kLifespan);
	
	static VertexBuffer vertices = {
		Vertex{ 1,    0,    0, Color(0, 255, 255)},
		Vertex{ 0.5,  0.4,  0, Color(0, 64,  255)},
		Vertex{-1,    0,    0, Color(0, 255, 255)},
		Vertex{ 0.5, -0.4,  0, Color(0, 64,  255)}
	};

	Core::graphics.PushMatrix();
	Core::graphics.Translate(_currPosition);
	Core::graphics.Rotate(_angle);

	vertices[0].color.alpha = 0;
	vertices[1].color.alpha = _alpha * 0.5;
	vertices[2].color.alpha = _alpha;
	vertices[3].color.alpha = _alpha * 0.5;

	const auto scale = _radius + _radius * _time / kLifespan;
	Core::graphics.Scale(scale);
	Core::graphics.Draw(vertices, DrawMode::TriangleFan);

	vertices[0].color.alpha = _alpha;
	vertices[1].color.alpha = _alpha;
	vertices[2].color.alpha = _alpha;
	vertices[3].color.alpha = _alpha;

	Core::graphics.Scale(0.5);
	Core::graphics.SetBlendMode(BlendMode::Add);
	Core::graphics.Draw(vertices, DrawMode::TriangleFan);
	Core::graphics.SetBlendMode(BlendMode::Alpha);
	
	Core::graphics.PopMatrix();
}

void Shot::Update(float deltaTime) {
	Assert(_time <= kLifespan);

	_time += deltaTime;
	if (_time > kLifespan) {
		deltaTime -= kLifespan - _time;
		_time = kLifespan;
	}

	_alpha = 255 - 223 * Core::EaseInQuint(_time / kLifespan);
	_velocity += _velocity * deltaTime;

	_prevPosition = _currPosition;
	_currPosition += _velocity * deltaTime;
}

} /* namespace Asteroids */