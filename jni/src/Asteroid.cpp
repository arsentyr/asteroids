#include "Asteroid.h"

namespace Asteroids {

Asteroid::Asteroid() {
	Color color;
	switch (Random::NextInt(0, 5)) {
	case 0: color = Color{29,  249, 20,  92 }; break; 	// electric lime
	case 1: color = Color{252, 126, 253, 104}; break;	// shocking pink
	case 2: color = Color{116, 66,  216, 104}; break;	// purple heart
	case 3: color = Color{255, 127, 73,  104}; break;	// burnt orange
	case 4: color = Color{253, 252, 116, 92 }; break;	// laser lemon
	case 5: color = Color{255, 73,  108, 104}; break;	// radical red
	}

	switch (Random::NextInt(0, 2)) {
	case 0:
		_shape = Shape::Triangle; 
		_vertices = {
			Vertex{1,                  0,                  0, color},
			Vertex{Cos(DegToRad(120)), Sin(DegToRad(120)), 0, color},
			Vertex{Cos(DegToRad(240)), Sin(DegToRad(240)), 0, color}
		};
		break;
	
	case 1:
		_shape = Shape::Quadrate;
		_vertices = {
			Vertex{ 1,  0, 0, color},
			Vertex{ 0,  1, 0, color},
			Vertex{-1,  0, 0, color},
			Vertex{ 0, -1, 0, color}
		};
		break;

	case 2:
		_shape = Shape::Rhomb;
		_vertices = {
			Vertex{ 1,  0,   0, color},
			Vertex{ 0,  0.5, 0, color},
			Vertex{-1,  0,   0, color},
			Vertex{ 0, -0.5, 0, color}
		};
		break;
	}

	_fragments.reserve(_numFragments);
	Reset(0.5, Vector2{0, 0} , Vector2{0, 0});
}

void Asteroid::Reset(float radius, const Vector2& position, const Vector2& velocity) {
	Assert(radius > 0);

	_state = State::Created;
	_radius = radius;
	_numFragments = _fragments.size();

	auto deltaAngle = 0.f;
	switch (_shape) {
	case Shape::Triangle:
		deltaAngle = 120.f / _numFragments;
		break;

	case Shape::Quadrate:
		deltaAngle = 90.f / _numFragments;
		break;

	case Shape::Rhomb:
		deltaAngle = 180.f / _numFragments;
		break;
	}
	
	auto angle = Random::NextFloat(0, 180);
	for (auto& fragment : _fragments) {
		fragment.Reset(angle, radius, position, velocity);
		angle += deltaAngle;
	}
}

void Asteroid::Scale(float scaleRadius, const Vector2& scaleAxis) {
	Assert(scaleRadius > 0);
	Assert(_state != State::Finished);

	_radius *= scaleRadius;

	for (auto& fragment : _fragments) {
		if (fragment.IsDrawable()) {
			fragment.Scale(scaleRadius, scaleAxis);
		}
	}
}

bool Asteroid::IsFinished() const {
	return _state == State::Finished;
}

void Asteroid::Draw() const {
	Assert(_state != State::Finished);

	for (auto& fragment : _fragments) {
		fragment.Draw();
	}
}

void Asteroid::Update(float deltaTime) {
	if (_state == State::Starting) {
		_state = State::Started;
		
		const auto length = _fragments.front().GetVelocity().Length();
		for (auto& fragment : _fragments) {
			Assert(fragment.IsDrawable());
			
			const auto radius = fragment.GetRadius() * Random::NextFloat(0.3, 0.75);
			fragment.SetRadius(radius, 0.15);

			const auto angle = fragment.GetAngle(); 
			fragment.SetVelocity(Vector2{
				static_cast<float>(length * Cos(DegToRad(angle))),
				static_cast<float>(length * Sin(DegToRad(angle)))
			});
			
			fragment.Resume();
		}
	}
}

void Asteroid::OnFragmentHit() {
	Assert(_numFragments > 0);

	if (_state == State::Created) {
		_state = State::Starting;
		for (auto& fragment : _fragments) {
			fragment.Start();
		}	
	}
}

void Asteroid::OnFragmentFinished() {
	Assert(_numFragments > 0);

	if (_numFragments > 0) {
		--_numFragments;
		_state = (_numFragments > 0) ? State::Finishing : State::Finished;
	}
}

Asteroid::Fragment::Fragment(Asteroid& asteroid)
	: _asteroid(asteroid)
{}

void Asteroid::Fragment::Reset(float angle, float radius, const Vector2& position, const Vector2& velocity) {
	_state = State::Created;

	SetAngle(angle);
	SetRadius(radius);
	SetPosition(position);
	SetVelocity(velocity);
}

void Asteroid::Fragment::Start() {
	Assert(_state == State::Created);
	_state = State::Starting;
}

void Asteroid::Fragment::Resume() {
	Assert(_state == State::Starting);
	_state = State::Started;
}

void Asteroid::Fragment::Scale(float scaleRadius, const Vector2& scaleAxis) {
	Assert(scaleRadius > 0);
	Assert(_state != State::Finished);

	const auto radiusInitial = scaleRadius * _radius.GetInitial();
	const auto radiusEnd = scaleRadius * _radius.GetEnd();
	_radius.SetInitial(radiusInitial);
	_radius.SetEnd(radiusEnd);
	if (_radius.IsTweening()) {
		_radius.Update(0);
	}
	else {
		_radius.Resume();
		_radius.Update(0);
		_radius.Pause();
	}

	_prevPosition *= scaleAxis;
	_currPosition *= scaleAxis;
	_velocity *= scaleAxis;	
}

void Asteroid::Fragment::SetAngle(float angle) {
	Assert(_state != State::Finishing);

	angle = fmod(angle, 360);

	_angle.SetInitial(angle);
	_angle.SetEnd(angle + 360);
	_angle.SetTime(0);
	_angle.Update(0);
}

float Asteroid::Fragment::GetAngle() const {
	return _angle.Value();
}

void Asteroid::Fragment::SetRadius(float radius, float duration) {
	Assert(radius > 0);
	Assert(duration >= 0);
	Assert(_state != State::Finishing);

	if (duration > 0) {
		_radius.SetInitial(_radius.Value());
		_radius.SetEnd(radius);
		_radius.SetTime(0);
		_radius.SetDuration(duration);
		_radius.Resume();
		_radius.Update(0);
	}
	else {
		_radius.SetInitial(radius);
		_radius.SetEnd(radius);
		_radius.SetTime(0);
		_radius.Resume();
		_radius.Update(0);
		_radius.Pause();
	}
}

float Asteroid::Fragment::GetRadius() const {
	return _radius.Value();
}

void Asteroid::Fragment::SetPosition(const Vector2& position) {
	_prevPosition = position;
	_currPosition = position;
}

Vector2 Asteroid::Fragment::GetPosition() const {
	return _currPosition;
}

void Asteroid::Fragment::SetVelocity(const Vector2& velocity) {
	_velocity = velocity;
}

Vector2 Asteroid::Fragment::GetVelocity() const {
	return _velocity;
}

bool Asteroid::Fragment::IsDrawable() const {
	return _state != State::Finished;
}

bool Asteroid::Fragment::Hit(const Vector2& startPosition, const Vector2& endPosition, float radius) {
	Assert(IsDrawable());

	bool hasHit = false;
	switch (_state) {
	case State::Created:
		hasHit = CircleCircleSweepTest(startPosition, endPosition, radius, _prevPosition, _currPosition, GetRadius());
		if (hasHit) {
			_asteroid.OnFragmentHit();
		}
		return hasHit;

	case State::Started:
		hasHit = CircleCircleSweepTest(startPosition, endPosition, radius, _prevPosition, _currPosition, GetRadius());
		if (hasHit) {
			SetAngle(GetAngle());
			_angle.SetEnd(3600 * _angle.GetDuration());
			SetRadius(0.0001, kDurationFinishing);
			_radius.SetEquation(Core::EasingBack{Core::EaseMode::In, 3});
			_state = State::Finishing;
		}
		return hasHit;

	default: return hasHit; 
	}
}

void Asteroid::Fragment::Clip(int width, int height) {
	switch (_state) {
	case State::Created: {
	case State::Starting:
	case State::Started:
			const auto radius = _radius.Value();		
			if ((_currPosition.x - _prevPosition.x) > 0) {
				if ((_currPosition.x - radius) > width && (_prevPosition.x - radius) <= width) {
					_currPosition.x = -radius;
				}
			}
			else if ((_currPosition.x + radius) < 0 && (_prevPosition.x + radius) >= 0) {
				_currPosition.x = width + radius;
			}
			
			if ((_currPosition.y - _prevPosition.y) > 0) {
				if ((_currPosition.y - radius) > height && (_prevPosition.y - radius) <= height) {
					_currPosition.y = -radius;
				}
			}
			else if ((_currPosition.y + radius) < 0 && (_prevPosition.y + radius) >= 0) {
				_currPosition.y = height + radius;
			}
		}
		break;

	default: break;
	}
}

void Asteroid::Fragment::Draw() const {
	if (_state == State::Finished) {
		return;
	}

	auto& vertices = _asteroid._vertices;

	const auto alpha = vertices[0].color.alpha;
	if (_state == State::Finishing) {
		const auto a = alpha - (alpha * _radius.GetTime()) / _radius.GetDuration();
		for (std::size_t index = 0; index < vertices.Count(); ++index) {
			vertices[index].color.alpha = a; 
		}
	}

	Core::graphics.PushMatrix();
	Core::graphics.Translate(GetPosition());
	Core::graphics.Rotate(GetAngle());
	Core::graphics.Scale(GetRadius());
	Core::graphics.SetBlendMode(BlendMode::Add);
	Core::graphics.SetLineWidth(3);
	Core::graphics.Draw(vertices, DrawMode::LineLoop);
	Core::graphics.SetLineWidth(2);
	Core::graphics.Draw(vertices, DrawMode::LineLoop);
	Core::graphics.SetLineWidth(1);
	Core::graphics.Draw(vertices, DrawMode::LineLoop);
	Core::graphics.SetBlendMode(BlendMode::Alpha);
	Core::graphics.Draw(vertices, DrawMode::TriangleFan);
	Core::graphics.PopMatrix();

	if (_state == State::Finishing) {
		for (std::size_t index = 0; index < vertices.Count(); ++index) {
			vertices[index].color.alpha = alpha; 
		}
	}
}

bool Asteroid::Fragment::Update(float deltaTime) {
	switch (_state) {
	case State::Created:
	case State::Started:
		_angle.Update(deltaTime);
		_radius.Update(deltaTime);
		_prevPosition = _currPosition;
		_currPosition += _velocity * deltaTime;
		Assert(_prevPosition != _currPosition);
		return true;

	case State::Finishing:
		_angle.Update(deltaTime);
		_radius.Update(deltaTime);
		if (_radius.IsFinished()) {
			_state = State::Finished;
			SetAngle(0);
			_radius.SetEquation(TweenEasing::CubicOut);
			_asteroid.OnFragmentFinished();
		}
		return false;

	default: return false;
	}
}

bool Asteroid::Fragment::CircleCircleSweepTest(const Vector2& firstStartPosition,
		const Vector2& firstEndPosition, float firstRadius,
		const Vector2& secondStartPosition, const Vector2& secondEndPosition,
		float secondRadius) {
	Assert(firstRadius > 0 && secondRadius > 0);
	
	const auto start = secondStartPosition - firstStartPosition;
	const auto change = secondEndPosition - firstEndPosition - start;
	const auto radius = firstRadius + secondRadius;
	const auto a = change.DotProduct(change);
	const auto b = 2 * start.DotProduct(change);
	const auto c = start.DotProduct(start) - radius * radius;
	if (a != 0) {
		const auto d = b * b - 4 * a * c;
		if (d < 0) {
			return false;
		}
		else if (d > 0) {
			const auto dsqr = std::sqrt(d);
			const auto t1 = (dsqr - b) / (2 * a);
			if (t1 >= 0 && t1 <= 1) {
				return true;
			}
			const auto t2 = (-dsqr - b) / (2 * a);
			return t2 >= 0 && t2 <= 1;
		}
		else {
			const auto t = -b / (2 * a);
			return t >= 0 && t <= 1;
		}
	}
	else if (b != 0) {
		const auto t = -c / b;
		return t >= 0 && t <= 1;
	}
	else {
		return Abs(c) < 0.001;
	}
}

Asteroid::ForwardIterator::ForwardIterator(Fragments& fragments, std::size_t position)
		: _fragments(fragments)
		, _position{position} {
	Assert(_position >= 0 && _position <= _fragments.size());
}

bool Asteroid::ForwardIterator::operator!=(const ForwardIterator& iterator) const {
	return _position != iterator._position;
}

Asteroid::ForwardIterator& Asteroid::ForwardIterator::operator++() {
	Assert(_position < _fragments.size());
	++_position;
	return *this;
}

Asteroid::Fragment& Asteroid::ForwardIterator::operator*() {
	Assert(_position < _fragments.size());
	return _fragments[_position];
}

Asteroid::ForwardIterator Asteroid::begin() {
	return ForwardIterator{_fragments, 0};
}

const Asteroid::ForwardIterator Asteroid::end() {
	return ForwardIterator{_fragments, _fragments.size()};
}

} /* namespace Asteroids */