#include "Application.h"

#include <limits>
#include "Core/Core.h"

namespace Asteroids {

Application& Application::Instance() {
	static Application application;
	return application;
}

void Application::Initialize() {
	Assert(_state == State::Default);
	if (_state != State::Default) {
		Core::log.Error("Initialize failed: application already initialized");
		return;
	}

	Core::log.Debug("Initialize");
	Core::graphics.Initialize();
	Core::graphics.SetBlend(true);
	Core::graphics.SetBlendMode(BlendMode::Alpha);
	Core::graphics.SetDepthTest(false);
	_state = State::Initialized;
}

void Application::ResetGraphicsResources() {
	Assert(_state != State::Default);
	if (_state == State::Default) {
		Core::log.Error("Reset graphics resources failed: application not initialized");
		return;
	}

	Core::log.Debug("ResetGraphicsResources");
	Core::graphics.Reset();
}

void Application::Resize(int width, int height) {
	Assert(width > 0 && height > 0);
	Assert(_state != State::Default);
	if (_state == State::Default) {
		Core::log.Error("Resize failed: application not initialized");
		return;
	}

	Core::log.Debug("Resize: width=%d, height=%d", width, height);
	Core::graphics.SetViewport(-width, -height, width + width, height + height);
	Core::graphics.SetOrtho(-width, width, -height, height, 1, -1);
	_space.Resize(width, height);
}

void Application::Resume() {
	Assert(_state == State::Initialized || _state == State::Paused);
	if (_state != State::Initialized && _state != State::Paused) {
		Core::log.Error("Resume application failed: incorrect state=%d", _state);
		return;
	}

	Core::log.Debug("Resume");
	Core::timer.Start();
	_state = State::Resumed;	
}

void Application::DrawFrame() {
	Assert(_state == State::Resumed);
	if (_state != State::Resumed) {
		Core::log.Error("Draw frame failed: application not resumed");
		return;
	}

	auto deltaTime = Core::timer.GetDeltaTime();
	Assert(deltaTime >= 0);
	constexpr float kLimitDeltaTime = 0.5;
	if (deltaTime > kLimitDeltaTime) {
		deltaTime = kLimitDeltaTime;
	}

	Core::graphics.BeginFrame();

	if (deltaTime > 0) {
		_space.Update(deltaTime);
	}
	_space.Draw();

#ifndef NDEBUG
	if (IsDevelop()) {
		const auto width = _space.GetWidth();
		const auto height = _space.GetHeight();
		const auto x = width * 0.025;
		const auto y = height - height * 0.05;
		const auto fontSize = height * 0.025; 
		DrawFps(x, y, fontSize, deltaTime);
	}
#endif

	Core::graphics.EndFrame();
}

void Application::Pause() {
	Assert(_state == State::Initialized || _state == State::Resumed);
	if (_state != State::Initialized && _state != State::Resumed) {
		Core::log.Error("Pause application failed: incorrect state=%d", _state);
		return;
	}

	Core::log.Debug("Pause");
	Core::timer.Stop();
	_state = State::Paused;
}

void Application::Tap(float x, float y) {
	Assert(_state == State::Resumed);
	if (_state == State::Resumed) {
		_space.Tap(x, y);
	}
}

void Application::Pan(float dx, float dy) {
	Assert(dx != 0 || dy != 0);
	Assert(_state == State::Resumed);
	if (_state == State::Resumed) {
		_space.Pan(dx, dy);
	}
}

void Application::Develop(bool isDevelop) {
	_isDevelop = isDevelop;
}

bool Application::IsDevelop() const {
	return _isDevelop;
}

void Application::DrawFps(float x, float y, float fontSize, float deltaTime) {
	Assert(x >= 0 && y >= 0);
	Assert(fontSize > 0);
	Assert(deltaTime >= 0);

	static float elapsedTime = 0;
	static int numFrames = 0;
	static int fps = 0;
	elapsedTime += deltaTime;
	++numFrames;
	constexpr float kMaxElapsedTime = 3;
	if (elapsedTime > kMaxElapsedTime) {
		fps = numFrames / elapsedTime;
		elapsedTime = 0;
		numFrames = 0;
	}

	static const VertexBuffer zero = {
		Vertex{0,   0, 0, Color::Red}, 
		Vertex{0,   1, 0, Color::Red},
		Vertex{0.5, 1, 0, Color::Red},
		Vertex{0.5, 0, 0, Color::Red},
	};
	static const VertexBuffer one = {
		Vertex{0,   0.5, 0, Color::Red},
		Vertex{0.5, 1,   0, Color::Red},
		Vertex{0.5, 0,   0, Color::Red}
	};
	static const VertexBuffer two = {
		Vertex{0,   1,   0, Color::Red},
		Vertex{0.5, 1,   0, Color::Red},
		Vertex{0.5, 0.5, 0, Color::Red},
		Vertex{0,   0,   0, Color::Red},
		Vertex{0.5, 0,   0, Color::Red}
	};
	static const VertexBuffer three = {
		Vertex{0,   1,   0, Color::Red},
		Vertex{0.5, 1,   0, Color::Red},
		Vertex{0,   0.5, 0, Color::Red},
		Vertex{0.5, 0.5, 0, Color::Red},
		Vertex{0,   0,   0, Color::Red}
	};
	static const VertexBuffer four = {
		Vertex{0,   1,   0, Color::Red},
		Vertex{0,   0.5, 0, Color::Red},
		Vertex{0.5, 0.5, 0, Color::Red},
		Vertex{0.5, 1,   0, Color::Red},
		Vertex{0.5, 0,   0, Color::Red}
	};
	static const VertexBuffer five = {
		Vertex{0,   0,   0, Color::Red},
		Vertex{0.5, 0,   0, Color::Red},
		Vertex{0.5, 0.5, 0, Color::Red},
		Vertex{0,   0.5, 0, Color::Red},
		Vertex{0,   1,   0, Color::Red},
		Vertex{0.5, 1,   0, Color::Red}
	};
	static const VertexBuffer six = {
		Vertex{0.5, 1,   0, Color::Red},
		Vertex{0,   0.5, 0, Color::Red},
		Vertex{0,   0,   0, Color::Red},
		Vertex{0.5, 0,   0, Color::Red},
		Vertex{0.5, 0.5, 0, Color::Red},
		Vertex{0,   0.5, 0, Color::Red}
	};
	static const VertexBuffer seven = {
		Vertex{0,   1, 0, Color::Red},
		Vertex{0.5, 1, 0, Color::Red},
		Vertex{0,   0, 0, Color::Red}
	};
	static const VertexBuffer eight = {
		Vertex{0,   0.5, 0, Color::Red},
		Vertex{0,   1,   0, Color::Red},
		Vertex{0.5, 1,   0, Color::Red},
		Vertex{0.5, 0.5, 0, Color::Red},
		Vertex{0,   0.5, 0, Color::Red},
		Vertex{0,   0,   0, Color::Red},
		Vertex{0.5, 0,   0, Color::Red},
		Vertex{0.5, 0.5, 0, Color::Red}
	};
	static const VertexBuffer nine = {
		Vertex{0,   0,   0, Color::Red},
		Vertex{0.5, 0.5, 0, Color::Red},
		Vertex{0.5, 1,   0, Color::Red},
		Vertex{0,   1,   0, Color::Red},
		Vertex{0,   0.5, 0, Color::Red},
		Vertex{0.5, 0.5, 0, Color::Red}
	};

	Core::graphics.PushMatrix();
	Core::graphics.Translate(x + fontSize, y);
	Core::graphics.Scale(fontSize);
	for (auto result = div(fps, 10);; result = div(result.quot, 10)) {
		switch (result.rem) {
		case 0: Core::graphics.Draw(zero, DrawMode::LineLoop); break;
		case 1: Core::graphics.Draw(one, DrawMode::LineStrip); break;
		case 2: Core::graphics.Draw(two, DrawMode::LineStrip); break;
		case 3: Core::graphics.Draw(three, DrawMode::LineStrip); break;
		case 4: Core::graphics.Draw(four, DrawMode::LineStrip); break;
		case 5: Core::graphics.Draw(five, DrawMode::LineStrip); break;		
		case 6: Core::graphics.Draw(six, DrawMode::LineLoop); break;
		case 7: Core::graphics.Draw(seven, DrawMode::LineStrip); break;
		case 8: Core::graphics.Draw(eight, DrawMode::LineStrip); break;
		case 9: Core::graphics.Draw(nine, DrawMode::LineStrip); break;
		}
		if (result.quot == 0) {
			break;
		}
		Core::graphics.Translate(-1, 0);
	}
	Core::graphics.PopMatrix();
}

} /* namespace Asteroids */