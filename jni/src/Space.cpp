#include "Space.h"

#include <vector>
#include "Application.h"
#include "Asteroid.h"

namespace Asteroids {

Space::Space() {
	Reset();
}

void Space::Reset() {
	_spaceship.Reset(_minRadiusSpaceship, Vector2{_width / 2.f, _height / 2.f}, Vector2{0, _maxVelocitySpaceship});

	for (auto& asteroid : _asteroids) {
		DeleteAsteroid(asteroid);
	}
	while (_numAsteroids < kMinNumberOfAsteroids) {
		GenerateAsteroid();
	}

	for (auto& shot : _shots) {
		_shots.Delete(shot);
	}
}

void Space::Resize(int width, int height) {
	Assert(width > 0 && height > 0);

	const auto scale = static_cast<float>(Min(width, height)) / Min(_width, _height);
	const auto scaleAxis = Vector2{static_cast<float>(width) / _width, static_cast<float>(height) / _height};

	_width = width;
	_height = height;

	// FIXME: take into account the DPI of the scaling 
	_minRadiusSpaceship *= scale;
	_maxVelocitySpaceship *= scale;

	_minRadiusAsteroid *= scale;
	_maxRadiusAsteroid *= scale;

	_minVelocityAsteroid *= scale;
	_maxVelocityAsteroid *= scale;

	_maxRadiusShot *= scale;
	_minVelocityShot *= scale;

	_spaceship.Scale(scale, scaleAxis);

	for (auto& shot : _shots) {
		shot.Scale(scale, scaleAxis);
	}

	for (auto& asteroid : _asteroids) {
		asteroid.Scale(scale, scaleAxis);
	}
}

int Space::GetWidth() const {
	return _width;
}

int Space::GetHeight() const {
	return _height;
}

void Space::Draw() const {
	static const VertexBuffer background = {
		Vertex{ 1.1, -0.1, 0, Color(255, 0, 255, 32)},
		Vertex{ 1.1,  1.1, 0, Color(255, 0, 255, 32)},
		Vertex{-0.1,  1.1, 0, Color(255, 0, 255, 32)},
		Vertex{-0.1, -0.1, 0, Color(255, 0, 255, 32)}
	};
	
	Core::graphics.PushMatrix();
	Core::graphics.Scale(Max(_width, _height));
	Core::graphics.Draw(background, DrawMode::TriangleFan);
	Core::graphics.PopMatrix();

	for (const auto& shot : _shots) {
		shot.Draw();
	}

	for (const auto& asteroid : _asteroids) {
		asteroid.Draw();
	}
	
	_spaceship.Draw();

#ifndef NDEBUG
	if (Application::Instance().IsDevelop()) {
		DrawDevelop();
	}
#endif
}

void Space::Update(float deltaTime) {
	Assert(deltaTime > 0);

	const auto spaceshipRadius = _spaceship.GetRadius();
	const auto spaceshipStartPosition = _spaceship.GetPosition();
	_spaceship.Update(deltaTime);
	_spaceship.Clip(_width, _height);
	const auto spaceshipEndPosition = _spaceship.GetPosition();

	for (auto& shot : _shots) {
		shot.Update(deltaTime);
	}

	for (auto& asteroid : _asteroids) {
		asteroid.Update(deltaTime);
		
		for (auto& fragment : asteroid) {
			if (fragment.Update(deltaTime)) {
				for (auto& shot : _shots) {
					const auto shotRadius = shot.GetRadius();
					const auto shotStartPosition = shot.GetPrevPosition();	
					const auto shotEndPosition = shot.GetCurrPosition();
					if (fragment.Hit(shotStartPosition, shotEndPosition, shotRadius)) {
						_shots.Delete(shot);
						break;
					}
				}

				if (fragment.Hit(spaceshipStartPosition, spaceshipEndPosition, spaceshipRadius)) {
					Reset();
					return;
				}

				fragment.Clip(_width, _height);
			}
		}
		
		if (asteroid.IsFinished()) {
			DeleteAsteroid(asteroid);
		}
	}

	for (auto& shot : _shots) {
		if (shot.IsFinished()) {
			_shots.Delete(shot);
		}
		else {
			shot.Clip(_width, _height);
		}
	}

	_delayAsteroid -= deltaTime;
	if (_delayAsteroid <= 0) {
		_delayAsteroid = Random::NextFloat(kMinDelayAsteroid, kMaxDelayAsteroid);
		GenerateAsteroid();
	}
	while (_numAsteroids < kMinNumberOfAsteroids) {
		GenerateAsteroid();
	}
}

void Space::Tap(float x, float y) {
	Assert(x >= 0 && x <= _width && y >= 0 && y <= _height);
	
	y = _height - y;

	const auto spaceshipPosition = _spaceship.GetPosition();
	const auto spaceshipRadius = _spaceship.GetRadius();
	if (x >= (spaceshipPosition.x - spaceshipRadius) && x <= (spaceshipPosition.x + spaceshipRadius)
			&& y >= (spaceshipPosition.y - spaceshipRadius) && y <= (spaceshipPosition.y + spaceshipRadius)) {
		_spaceship.Stop();

		const auto isDevelop = !Application::Instance().IsDevelop();
		Application::Instance().Develop(isDevelop);
	}
	else {
		Shot& shot = _shots.Take();

		const auto angle = DegToRad(_spaceship.GetAngle());
		const auto sine = Sin(angle);
		const auto cosine = Cos(angle);
		const auto offset = spaceshipRadius + _maxRadiusShot / 2;
		const auto position = spaceshipPosition + Vector2{
			static_cast<float>(offset * cosine),
			static_cast<float>(offset * sine)
		};

		const auto velocity = Vector2{
			static_cast<float>(_minVelocityShot * cosine),
			static_cast<float>(_minVelocityShot * sine)
		};
		
		shot.Reset(_maxRadiusShot, position, velocity);
	}
}

void Space::Pan(float dx, float dy) {
	Assert(dx != 0 || dy != 0);

	// FIXME: take into account the DPI and ratio of the scaling 
	const auto deltaVelocity = Vector2{1.75f * dx, -1.75f * dy};

	const auto startVelocity = _spaceship.GetVelocity();
	auto endVelocity = startVelocity + deltaVelocity;
	if (endVelocity.LengthSq() > Sq(_maxVelocitySpaceship)) {
		endVelocity = endVelocity.Normalize() * _maxVelocitySpaceship;
	}

	_spaceship.SetVelocity(endVelocity, kMinDurationTweenVelocitySpacehip);
}

void Space::GenerateAsteroid() {
	auto& asteroid = _asteroids.Take();
	++_numAsteroids;

	const auto radius = Random::NextFloat(_minRadiusAsteroid, _maxRadiusAsteroid);
	
	const auto spaceshipRadius = _spaceship.GetRadius();
	const auto spaceshipPosition = _spaceship.GetPosition();
	Vector2 target{
		spaceshipPosition.x + Random::NextFloat(-spaceshipRadius, spaceshipRadius),
		spaceshipPosition.y + Random::NextFloat(-spaceshipRadius, spaceshipRadius)
	};
	if ((target.x - radius) <= 0) {
		target.x = spaceshipPosition.x + spaceshipRadius; 
	}
	else if ((target.x + radius) >= _width) {
		target.x = spaceshipPosition.x - spaceshipRadius;
	}
	if ((target.y - radius) <= 0) {
		target.y = spaceshipPosition.y + spaceshipRadius; 
	}
	else if ((target.y + radius) >= _height) {
		target.y = spaceshipPosition.y - spaceshipRadius;
	}

	const auto center = Vector2{_width / 2.f, _height / 2.f};
	const auto positionDirection = Vector2{Random::NextFloat(-1, 1), Random::NextFloat(-1, 1)}.Normalize();
	const auto positionOffset = (center + radius).Length();
	const auto position = center + positionDirection * positionOffset;
	Assert((position.x + radius) < 0 || (position.x - radius) > _width
		|| (position.y + radius) < 0 || (position.y - radius) > _height);

	const auto velocityLength = Random::NextFloat(_minVelocityAsteroid, _maxVelocityAsteroid);
	const auto velocityDirection = (target - position).Normalize();
	const auto velocity = velocityDirection * velocityLength;

	asteroid.Reset(radius, position, velocity);
}

void Space::DeleteAsteroid(const Asteroid& asteroid) {
	_asteroids.Delete(asteroid);
	--_numAsteroids;
}

void Space::DrawDevelop() const {
	static const VertexBuffer circle {
		Vertex{ 1,    0.5, 0, Color::White},
		Vertex{ 0.5,  1,   0, Color::White},
		Vertex{-0.5,  1,   0, Color::White},
		Vertex{-1,    0.5, 0, Color::White},
		Vertex{-1,   -0.5, 0, Color::White},
		Vertex{-0.5, -1,   0, Color::White},
		Vertex{ 0.5, -1,   0, Color::White},
		Vertex{ 1,   -0.5, 0, Color::White},
	};
	static VertexBuffer line {
		Vertex{0, 0, 0, Color::Red},
		Vertex{0, 0, 0, Color::Red}
	};
	auto& endLine = line[1];

	endLine.x = _spaceship.GetVelocity().x;
	endLine.y = _spaceship.GetVelocity().y;
	Core::graphics.PushMatrix();
	Core::graphics.Translate(_spaceship.GetPosition());
	Core::graphics.Draw(line, DrawMode::Lines);
	Core::graphics.Scale(_spaceship.GetRadius());
	Core::graphics.Draw(circle, DrawMode::LineLoop);
	Core::graphics.PopMatrix();

	for (const auto& shot : _shots) {
		const auto velocity = shot.GetVelocity();
		endLine.x = velocity.x;
		endLine.y = velocity.y;

		Core::graphics.PushMatrix();
		Core::graphics.Translate(shot.GetCurrPosition());
		Core::graphics.Draw(line, DrawMode::Lines);
		Core::graphics.Scale(shot.GetRadius());
		Core::graphics.Draw(circle, DrawMode::LineLoop);
		Core::graphics.PopMatrix();
	}

	for (auto& asteroid : _asteroids) {
		for (const auto& fragment : asteroid) {
			if (fragment.IsDrawable()) {
				const auto velocity = fragment.GetVelocity();
				endLine.x = velocity.x;
				endLine.y = velocity.y;

				Core::graphics.PushMatrix();
				Core::graphics.Translate(fragment.GetPosition());
				Core::graphics.Draw(line, DrawMode::Lines);
				Core::graphics.Scale(fragment.GetRadius());
				Core::graphics.Draw(circle, DrawMode::LineLoop);
				Core::graphics.PopMatrix();
			}
		}
	}
}

} /* namespace Asteroids */