#ifndef CORE_TWEEN_H
#define CORE_TWEEN_H

#include <limits>
#include "Easing.h"
#include "TweenEquation.h"
#include "../Assert.h"
#include "../Math/Math.h"

namespace Core {

enum class TweenEasing {
	Linear,
	QuadIn,
	QuadOut,
	QuadInOut,
	QuadOutIn,
	CubicIn,
	CubicOut,
	CubicInOut,
	CubicOutIn,
	QuartIn,
	QuartOut,
	QuartInOut,
	QuartOutIn,
	QuintIn,
	QuintOut,
	QuintInOut,
	QuintOutIn,
	SineIn,
	SineOut,
	SineInOut,
	SineOutIn,
	ExpoIn,
	ExpoOut,
	ExpoInOut,
	ExpoOutIn,
	CircIn,
	CircOut,
	CircInOut,
	CircOutIn
};

enum class TweenMode {
	Forward,
	LoopForward,
	LoopPingPong
};

template<typename T>
class Tween {
public:
	Tween(const T& initial, const T& end, const TweenEquation& equation, float duration,
		TweenMode mode = TweenMode::Forward, bool isTweening = true)
			: _value{initial}
			, _initial{initial}
			, _equation{equation}
			, _mode{mode}
			, _isTweening{isTweening} {
		SetEnd(end);
		SetDuration(duration);
	}

	Tween(const T& initial, const T& end, TweenEasing easing, float duration,
		TweenMode mode = TweenMode::Forward, bool isTweening = true)
			: Tween{initial, end, CreateEquation(easing), duration, mode, isTweening}
	{}

	const T& Value() const {
		return _value;
	}

	void SetInitial(const T& initial) {
		_change += _initial - initial;
		_initial = initial;
	}

	T GetInitial() const {
		return _initial;
	}

	void SetEnd(const T& end) {
		_change = end - _initial;
	}

	T GetEnd() const {
		return _initial + _change;
	}

	void SetEquation(const TweenEquation& equation) {
		_equation = equation;
	}

	void SetEquation(TweenEasing easing) {
		_equation = CreateEquation(easing);
	}

	TweenEquation GetEquation() const {
		return _equation;
	}

	void SetDuration(float duration) {
		Assert(duration > 0);

		_duration = (duration > 0) ? duration : std::numeric_limits<float>::min();
	}

	float GetDuration() const {
		return _duration;
	}

	void SetTime(float time) {
		Assert(time >= 0 && time <= _duration);

		_time = (time < 0) ? 0 : ((time > _duration) ? _duration : time);
	}

	float GetTime() const {
		return _time;
	}

	void SetMode(TweenMode mode) {
		_mode = mode;
		switch (_mode) {
		case TweenMode::LoopForward:
		case TweenMode::LoopPingPong:
			_isFinished = false;
			break;
		}
	}

	TweenMode GetMode() const {
		return _mode;
	}

	void Pause() {
		if (_isTweening) {
			_isTweening = false;
		}
	}

	void Resume() {
		if (!_isTweening) {
			_isTweening = true;
			_isFinished = false;
		} 
	}
	
	bool Update(float deltaTime) {
		Assert(deltaTime >= 0);
		if (!_isTweening) {
			return false;
		}

		_time += deltaTime;
		switch (_mode) {
		case TweenMode::Forward:
			if (_time < 0) {
				_time = 0;
				_isTweening = false;
				_isFinished = false;
			}
			else if (_time > _duration) {
				_time = _duration;
				_isTweening = false;
				_isFinished = true;
			}
			break;

		case TweenMode::LoopForward:
			if (_time < 0) {
				_time -= _time;
				if (_time > _duration) {
					_time = fmod(_time, _duration);
				}

			}
			else if (_time > _duration) {
				_time -= _duration;
				if (_time > _duration) {
					_time = fmod(_time, _duration);
				}
			}
			break;

		case TweenMode::LoopPingPong:
			if (_time < 0) {
				_time = -_time;
				if (_time > _duration) {
					const int count = static_cast<int>(_time / _duration);
					_time -= count * _duration;
					if (count % 2 != 0) {
						_initial += _change;
						_change = -_change;
					}
				}
			}
			else if (_time > _duration) {
				_time -= _duration;
				if (_time <= _duration) {
					_initial += _change;
					_change = -_change;
				}
				else {
					const int count = static_cast<int>(_time / _duration);
					_time -= count * _duration;
					if (count % 2 == 0) {
						_initial += _change;
						_change = -_change;
					}
				}
			}
			break;
		}

		_value = _initial + _change * _equation(_time / _duration);
		
		return true;
	}
	
	bool IsTweening() const {
		return _isTweening;
	}

	bool IsFinished() const {
		return _isFinished;
	}

private:
	static TweenEquation CreateEquation(TweenEasing easing) {
		switch (easing) {
		case TweenEasing::Linear: return &EaseLinear;

		case TweenEasing::QuadIn: return &EaseInQuad;
		case TweenEasing::QuadOut: return &EaseOutQuad;
		case TweenEasing::QuadInOut: return &EaseInOutQuad;
		case TweenEasing::QuadOutIn: return &EaseOutInQuad;

		case TweenEasing::CubicIn: return &EaseInCubic;
		case TweenEasing::CubicOut: return &EaseOutCubic;
		case TweenEasing::CubicInOut: return &EaseInOutCubic;
		case TweenEasing::CubicOutIn: return &EaseOutInCubic;

		case TweenEasing::QuartIn: return &EaseInQuart;
		case TweenEasing::QuartOut: return &EaseOutQuart;
		case TweenEasing::QuartInOut: return &EaseInOutQuart;
		case TweenEasing::QuartOutIn: return &EaseOutInQuart;

		case TweenEasing::QuintIn: return &EaseInQuint;
		case TweenEasing::QuintOut: return &EaseOutQuint;
		case TweenEasing::QuintInOut: return &EaseInOutQuint;
		case TweenEasing::QuintOutIn: return &EaseOutInQuint;

		case TweenEasing::SineIn: return &EaseInSine;
		case TweenEasing::SineOut: return &EaseOutSine;
		case TweenEasing::SineInOut: return &EaseInOutSine;
		case TweenEasing::SineOutIn: return &EaseOutInSine;

		case TweenEasing::ExpoIn: return &EaseInExpo;
		case TweenEasing::ExpoOut: return &EaseOutExpo;
		case TweenEasing::ExpoInOut: return &EaseInOutExpo;
		case TweenEasing::ExpoOutIn: return &EaseOutInExpo;

		case TweenEasing::CircIn: return &EaseInCirc;
		case TweenEasing::CircOut: return &EaseOutCirc;
		case TweenEasing::CircInOut: return &EaseInOutCirc;
		case TweenEasing::CircOutIn: return &EaseOutInCirc;
		}
		Assert(false);
	}

private:
	T _value;
	T _initial;
	T _change;
	float _time = 0;
	float _duration = 1;
	TweenEquation _equation = &EaseLinear;
	TweenMode _mode = TweenMode::Forward;
	bool _isTweening = false;
	bool _isFinished = false;
};

} /* namespace Core */

#endif /* CORE_TWEEN_H */