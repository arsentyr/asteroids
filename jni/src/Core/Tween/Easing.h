#ifndef CORE_EASING_H
#define CORE_EASING_H

#include "../Assert.h"
#include "../Math/Math.h"

namespace Core {

static float EaseLinear(float t) {
	Assert(t >= 0 && t <= 1);
	return t;
}

static float EaseInQuad(float t) {
	Assert(t >= 0 && t <= 1);
	return t * t;
}

static float EaseOutQuad(float t) {
	Assert(t >= 0 && t <= 1);
	return -t * (t - 2);
}

static float EaseInOutQuad(float t) {
	Assert(t >= 0 && t <= 1);
	t += t;
	if (t < 1) {
		return t * t / 2;
	}
	else {
		--t;
		return -0.5 * (t * (t - 2) - 1);
	}
}

static float EaseOutInQuad(float t) {
	Assert(t >= 0 && t <= 1);
	return (t < 0.5) ? EaseOutQuad(t + t) / 2
	                 : EaseInQuad((t + t) - 1) / 2 + 0.5;
}

static float EaseInCubic(float t) {
	Assert(t >= 0 && t <= 1);
	return t * t * t;
}

static float EaseOutCubic(float t) {
	Assert(t >= 0 && t <= 1.001);
	--t;
	return t * t * t + 1;
}

static float EaseInOutCubic(float t) {
	Assert(t >= 0 && t <= 1);
	t += t;
	if (t < 1) {
		return 0.5 * t * t * t;
	}
	else {
		t -= 2;
		return 0.5 * (t * t * t + 2);
	}
}

static float EaseOutInCubic(float t) {
	Assert(t >= 0 && t <= 1);
	return (t < 0.5) ? EaseOutCubic(t + t) / 2
	                 : EaseInCubic(t + t - 1) / 2 + 0.5;
}

static float EaseInQuart(float t) {
	Assert(t >= 0 && t <= 1);
	return t * t * t * t;
}

static float EaseOutQuart(float t) {
	Assert(t >= 0 && t <= 1);
	--t;
	return -(t * t * t * t - 1);
}

static float EaseInOutQuart(float t) {
	Assert(t >= 0 && t <= 1);
	t += t;
	if (t < 1) {
		return 0.5 * t * t * t * t;
	}
	else {
		t -= 2;
		return -0.5 * (t * t * t * t - 2);
	}
}

static float EaseOutInQuart(float t) {
	Assert(t >= 0 && t <= 1);
	return (t < 0.5) ? EaseOutQuart(t + t) / 2
	                 : EaseInQuart(t + t - 1) / 2 + 0.5;
}

static float EaseInQuint(float t) {
	Assert(t >= 0 && t <= 1);
	return t * t * t * t * t;
}

static float EaseOutQuint(float t) {
	Assert(t >= 0 && t <= 1);
    --t;
    return t * t * t * t * t + 1;
}

static float EaseInOutQuint(float t) {
	Assert(t >= 0 && t <= 1);
	t += t;
	if (t < 1) {
		return 0.5 * t * t * t * t * t;
	}
	else {
		t -= 2;
		return 0.5 * (t * t * t * t * t + 2);
	}
}

static float EaseOutInQuint(float t) {
	Assert(t >= 0 && t <= 1);
	return (t < 0.5) ? EaseOutQuint(t + t) / 2
	                 : EaseInQuint(t + t - 1) / 2 + 0.5;
}

static float EaseInSine(float t) {
	Assert(t >= 0 && t <= 1);
	return (t == 1) ? 1 : -Cos(t * PI_2) + 1;
}

static float EaseOutSine(float t) {
	Assert(t >= 0 && t <= 1);
    return Sin(t * PI_2);
}

static float EaseInOutSine(float t) {
	Assert(t >= 0 && t <= 1);
	return -0.5 * (Cos(PI * t) - 1);
}

static float EaseOutInSine(float t) {
	Assert(t >= 0 && t <= 1);
    return (t < 0.5) ? EaseOutSine(t + t) / 2
	                 : EaseInSine(t + t - 1) / 2 + 0.5;
}

static float EaseInExpo(float t) {
	Assert(t >= 0 && t <= 1);
	return (t == 0 || t == 1) ? t
	                          : Pow(2.f, 10 * (t - 1)) - 0.001;
}

static float EaseOutExpo(float t) {
	Assert(t >= 0 && t <= 1);
	return (t == 1) ? 1
	                : 1.001 * (-Pow(2.f, -10 * t) + 1);
}

static float EaseInOutExpo(float t) {
	Assert(t >= 0 && t <= 1);
	if (t == 0 || t == 1) {
		return t;
	}
	else {
		t += t;
		return (t < 1) ? 0.5 * Pow(2.f, 10 * (t - 1)) - 0.0005
		               : 0.5 * 1.0005 * (-Pow(2.f, -10 * (t - 1)) + 2);
	}
}

static float EaseOutInExpo(float t) {
	Assert(t >= 0 && t <= 1);
	return (t < 0.5) ? EaseOutExpo(t + t) / 2
	                 : EaseInExpo(t + t - 1) / 2 + 0.5;
}

static float EaseInCirc(float t) {
	Assert(t >= 0 && t <= 1);
	return -(Sqrt(1 - t * t) - 1);
}

static float EaseOutCirc(float t) {
	Assert(t >= 0 && t <= 1);
	--t;
	return Sqrt(1 - t * t);
}

static float EaseInOutCirc(float t) {
	Assert(t >= 0 && t <= 1);
	t += t;
	if (t < 1) {
		return -0.5 * (Sqrt(1 - t * t) - 1);
	}
	else {
		t -= 2;
		return 0.5 * (Sqrt(1 - t * t) + 1);
    }
}
 
static float EaseOutInCirc(float t) {
	Assert(t >= 0 && t <= 1);
	return (t < 0.5) ? EaseOutCirc(t + t) / 2
	                 : EaseInCirc(t + t - 1) / 2 + 0.5;
}

enum class EaseMode {
	In,
	Out,
	InOut,
	OutIn
};

class EasingBack {
public:
	EasingBack(EaseMode mode, float back)
			: _mode{mode}
			, _back{back} {
		Assert(_back >= 0);
	}

	static float EaseIn(float t, float s) {
		Assert(t >= 0 && t <= 1);
		return t * t * ((s + 1) * t - s);
	}

	static float EaseOut(float t, float s) {
		Assert(t >= 0 && t <= 1);
		t -= 1;
    	return t * t * ((s + 1) * t + s) + 1;
	}

	static float EaseInOut(float t, float s) {
		Assert(t >= 0 && t <= 1);
		t += t;
		s *= 1.525;
    	if (t < 1) {
        	return 0.5 * (t * t * ((s + 1) * t - s));
    	}
    	else {
        	t -= 2;
        	return 0.5 * (t * t * ((s + 1) * t + s) + 2);
    	}
	}

	static float EaseOutIn(float t, float s) {
		Assert(t >= 0 && t <= 1);
		return (t < 0.5) ? EaseOut(t + t, s) / 2
		                 : EaseIn(t + t - 1, s) / 2 + 0.5;
	}

	float operator()(float t) const {
		switch (_mode) {
		default:
		case EaseMode::In: return EaseIn(t, _back);
		case EaseMode::Out: return EaseOut(t, _back);
		case EaseMode::InOut: return EaseInOut(t, _back);
		case EaseMode::OutIn: return EaseOutIn(t, _back);
		}
	}

private:
	EaseMode _mode;
	float _back;
};

} /* namespace Core */

#endif /* CORE_EASING_H */