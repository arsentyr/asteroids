#ifndef CORE_TWEEN_EQUATION_H
#define CORE_TWEEN_EQUATION_H

#include <functional>

namespace Core {

typedef std::function<float (float)> TweenEquation;

} /* namespace Core */

#endif /* CORE_TWEEN_EQUATION_H */