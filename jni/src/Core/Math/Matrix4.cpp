#include "Matrix4.h"

namespace Core {

const Matrix4 Matrix4::Zero{
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0
};

const Matrix4 Matrix4::Identity{
	1, 0, 0, 0,
	0, 1, 0, 0,
	0, 0, 1, 0,
	0, 0, 0, 1
};

Matrix4::Matrix4(float x0, float x1, float x2, float x3,
                 float y0, float y1, float y2, float y3,
                 float z0, float z1, float z2, float z3,
                 float w0, float w1, float w2, float w3)
	: x0{x0}, x1{x1}, x2{x2}, x3{x3}
	, y0{y0}, y1{y1}, y2{y2}, y3{y3}
	, z0{z0}, z1{z1}, z2{x2}, z3{z3}
	, w0{w0}, w1{w1}, w2{w2}, w3{w3}
{}

Matrix4::Matrix4(float scalar)
	: Matrix4{scalar, scalar, scalar, scalar,
	          scalar, scalar, scalar, scalar,
	          scalar, scalar, scalar, scalar,
	          scalar, scalar, scalar, scalar}
{}

Matrix4 operator*(const Matrix4& mat1, const Matrix4& mat2) {
	return Matrix4{
		mat1.x0 * mat2.x0 + mat1.x1 * mat2.y0 + mat1.x2 * mat2.z0 + mat1.x3 * mat2.w0,
		mat1.x0 * mat2.x1 + mat1.x1 * mat2.y1 + mat1.x2 * mat2.z1 + mat1.x3 * mat2.w1,
		mat1.x0 * mat2.x2 + mat1.x1 * mat2.y2 + mat1.x2 * mat2.z2 + mat1.x3 * mat2.w2,
		mat1.x0 * mat2.x3 + mat1.x1 * mat2.y3 + mat1.x2 * mat2.z3 + mat1.x3 * mat2.w3,
		mat1.y0 * mat2.x0 + mat1.y1 * mat2.y0 + mat1.y2 * mat2.z0 + mat1.y3 * mat2.w0,
		mat1.y0 * mat2.x1 + mat1.y1 * mat2.y1 + mat1.y2 * mat2.z1 + mat1.y3 * mat2.w1,
		mat1.y0 * mat2.x2 + mat1.y1 * mat2.y2 + mat1.y2 * mat2.z2 + mat1.y3 * mat2.w2,
		mat1.y0 * mat2.x3 + mat1.y1 * mat2.y3 + mat1.y2 * mat2.z3 + mat1.y3 * mat2.w3,
		mat1.z0 * mat2.x0 + mat1.z1 * mat2.y0 + mat1.z2 * mat2.z0 + mat1.z3 * mat2.w0,
		mat1.z0 * mat2.x1 + mat1.z1 * mat2.y1 + mat1.z2 * mat2.z1 + mat1.z3 * mat2.w1,
		mat1.z0 * mat2.x2 + mat1.z1 * mat2.y2 + mat1.z2 * mat2.z2 + mat1.z3 * mat2.w2,
		mat1.z0 * mat2.x3 + mat1.z1 * mat2.y3 + mat1.z2 * mat2.z3 + mat1.z3 * mat2.w3,
		mat1.w0 * mat2.x0 + mat1.w1 * mat2.y0 + mat1.w2 * mat2.z0 + mat1.w3 * mat2.w0,
		mat1.w0 * mat2.x1 + mat1.w1 * mat2.y1 + mat1.w2 * mat2.z1 + mat1.w3 * mat2.w1,
		mat1.w0 * mat2.x2 + mat1.w1 * mat2.y2 + mat1.w2 * mat2.z2 + mat1.w3 * mat2.w2,
		mat1.w0 * mat2.x3 + mat1.w1 * mat2.y3 + mat1.w2 * mat2.z3 + mat1.w3 * mat2.w3,
	};
}
	
} /* namespace Core */