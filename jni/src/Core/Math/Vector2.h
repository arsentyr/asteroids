#ifndef CORE_VECTOR2_H
#define CORE_VECTOR2_H

namespace Core {

class Vector2 {
public:
	float x;
	float y;

public:
	explicit Vector2(float scalar);
	explicit Vector2(float x = 0, float y = 0);

	Vector2 operator+() const;
	Vector2 operator-() const;

	Vector2 operator+(const Vector2& vec) const;

	Vector2& operator+=(float scalar);
	Vector2& operator+=(const Vector2& vec);

	Vector2 operator-(const Vector2& vec) const;

	Vector2& operator-=(float scalar);
	Vector2& operator-=(const Vector2& vec);

	Vector2 operator*(const Vector2& vec) const;

	Vector2& operator*=(float scalar);
	Vector2& operator*=(const Vector2& vec);

	Vector2 operator/(const Vector2& vec) const;

	Vector2& operator/=(float scalar);
	Vector2& operator/=(const Vector2& vec);

	bool operator==(const Vector2& vec) const;

	bool operator!=(const Vector2& vec) const;

	bool Equals(const Vector2& vec, float epsilon = 0.001f) const;

	float Length() const;

	float LengthSq() const;

	float Distance(const Vector2& vec) const;

	float DistanceSq(const Vector2& vec) const;

	float DotProduct(const Vector2& vec) const;

	Vector2 Normalize() const;

	Vector2 Rotate(float angle) const;
};

Vector2 operator+(const Vector2& vec, float scalar);
Vector2 operator+(float scalar, const Vector2& vec);

Vector2 operator-(const Vector2& vec, float scalar);
Vector2 operator-(float scalar, const Vector2& vec);

Vector2 operator*(const Vector2& vec, float scalar);
Vector2 operator*(float scalar, const Vector2& vec);

Vector2 operator/(const Vector2& vec, float scalar);
Vector2 operator/(float scalar, const Vector2& vec);

} /* namespace Core */

#endif /* CORE_VECTOR2_H */