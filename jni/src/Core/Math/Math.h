#ifndef CORE_MATH_H
#define CORE_MATH_H

#include <cmath>
#include "../Assert.h"

#ifndef PI
#	define PI 3.14159265358979323846
#else
#	warning "PI already defined"
#endif

#ifndef PI_2
#	define PI_2 (PI / 2)
#else
#	warning "PI_2 already defined"
#endif

#define Abs(value) std::abs(value)

#define Min(a, b) std::min(a, b)
#define Max(a, b) std::max(a, b)

#define Cos(radians) std::cos(radians)
#define Sin(radians) std::sin(radians)
#define Acos(radians) std::acos(radians)
#define Asin(radians) std::asin(radians)

#define Sqrt(value) std::sqrt(value)
#define Sq(value) (value * value)

#define Pow(base, exponent) std::pow(base, exponent)

#define RadToDeg(radians) ((radians) * 180.0 / PI)
#define DegToRad(degrees) ((degrees) * PI / 180.0)

template <typename T>
static T Lerp(const T& start, const T& end, float t) {
	Assert(t >= 0 && t <= 1);
	return start + (end - start) * t;
}

#endif /* CORE_MATH_H */