#ifndef CORE_MATRIX4_H
#define CORE_MATRIX4_H

namespace Core {

class Matrix4 {
public:
	static const Matrix4 Zero;
	static const Matrix4 Identity;

public:
	union {
		struct {
			float x0, x1, x2, x3;
			float y0, y1, y2, y3;
			float z0, z1, z2, z3;
			float w0, w1, w2, w3;
		};
		float vec[16];
		float mat[4][4];
	};

public:
	Matrix4(float scalar = 0);

	Matrix4(float x0, float x1, float x2, float x3,
	        float y0, float y1, float y2, float y3,
	        float z0, float z1, float z2, float z3,
	        float w0, float w1, float w2, float w3);
};

Matrix4 operator*(const Matrix4& mat1, const Matrix4& mat2);

} /* namespace Core */

#endif /* CORE_MATRIX4_H */