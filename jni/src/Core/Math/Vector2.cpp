#include "Vector2.h"

#include "Math.h"
#include "../Assert.h"

namespace Core {

Vector2::Vector2(float scalar)
	: x{scalar}
	, y{scalar}
{}

Vector2::Vector2(float x, float y)
	: x{x}
	, y{y}
{}

Vector2 Vector2::operator+() const {
	return *this;
}

Vector2 Vector2::operator-() const {
	return Vector2{-x, -y};
}

Vector2 Vector2::operator+(const Vector2& vec) const {
	return Vector2{x + vec.x, y + vec.y};
}

Vector2& Vector2::operator+=(float scalar) {
	x += scalar;
	y += scalar;
	return *this;
}

Vector2& Vector2::operator+=(const Vector2& vec) {
	x += vec.x;
	y += vec.y;
	return *this;
}

Vector2 Vector2::operator-(const Vector2& vec) const {
	return Vector2{x - vec.x, y - vec.y};
}

Vector2& Vector2::operator-=(float scalar) {
	x -= scalar;
	y -= scalar;
	return *this;
}

Vector2& Vector2::operator-=(const Vector2& vec) {
	x -= vec.x;
	y -= vec.y;
	return *this;
}

Vector2 Vector2::operator*(const Vector2& vec) const {
	return Vector2{x * vec.x, y * vec.y};
}

Vector2& Vector2::operator*=(float scalar) {
	x *= scalar;
	y *= scalar;
	return *this;
}

Vector2& Vector2::operator*=(const Vector2& vec) {
	x *= vec.x;
	y *= vec.y;
	return *this;
}

Vector2 Vector2::operator/(const Vector2& vec) const {
	Assert(vec.x != 0 && vec.y != 0);
	return (vec.x != 0 && vec.y != 0) ? Vector2{x / vec.x, y / vec.y} : *this;
}

Vector2& Vector2::operator/=(float scalar) {
	Assert(scalar != 0);
	if (scalar != 0) {
		x /= scalar;
		y /= scalar;
	}
	return *this;
}

Vector2& Vector2::operator/=(const Vector2& vec) {
	Assert(vec.x != 0 && vec.y != 0);
	if (vec.x != 0 && vec.y != 0) {
		x /= vec.x;
		y /= vec.y;
	}
	return *this;
}

bool Vector2::operator==(const Vector2& vec) const {
	return x == vec.x && y == vec.y;
}

bool Vector2::operator!=(const Vector2& vec) const {
	return !(*this == vec);
}

bool Vector2::Equals(const Vector2& vec, float epsilon) const {
	return (Abs(x - vec.x) <= epsilon) && (Abs(y - vec.y) <= epsilon);
}

float Vector2::Length() const {
	return Sqrt(LengthSq());
}

float Vector2::LengthSq() const {
	return Sq(x) + Sq(y);
}

float Vector2::Distance(const Vector2& vec) const {
	return Sqrt(DistanceSq(vec));
}  

float Vector2::DistanceSq(const Vector2& vec) const {
	return (*this - vec).LengthSq();
}

float Vector2::DotProduct(const Vector2& vec) const {
	return x * vec.x + y * vec.y;
}

Vector2 Vector2::Normalize() const {
	Assert(Length() != 0);
	const auto length = Length();
	return (length > 0) ? (*this / length) : *this;
}

Vector2 Vector2::Rotate(float angle) const {
	Assert(angle != 0);
	angle = DegToRad(angle);
	const auto sine = Sin(angle);
	const auto cosine = Cos(angle);
	return Vector2{x * cosine - y * sine, x * sine + y * cosine};
}

Vector2 operator+(const Vector2& vec, float scalar) {
	return Vector2{vec.x + scalar, vec.y + scalar};
}

Vector2 operator+(float scalar, const Vector2& vec) {
	return vec + scalar;
}

Vector2 operator-(const Vector2& vec, float scalar) {
	return Vector2{vec.x - scalar, vec.y - scalar};
}

Vector2 operator-(float scalar, const Vector2& vec) {
	return Vector2{scalar - vec.x, scalar - vec.y};
}

Vector2 operator*(const Vector2& vec, float scalar) {
	return Vector2{vec.x * scalar, vec.y * scalar};
}

Vector2 operator*(float scalar, const Vector2& vec) {
	return vec * scalar;
}

Vector2 operator/(const Vector2& vec, float scalar) {
	Assert(scalar != 0);
	return (scalar != 0) ? Vector2{vec.x / scalar, vec.y / scalar} : vec;
}

Vector2 operator/(float scalar, const Vector2& vec) {
	Assert(vec.x != 0 && vec.y != 0);
	return (vec.x != 0 && vec.y != 0) ? Vector2{scalar / vec.x, scalar / vec.y} : vec;
}

} /* namespace Core */