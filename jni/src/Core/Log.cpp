#include "Log.h"

#include <android/log.h>

namespace Core {

Log::Log(const std::string& tag, LogLevel level)
	: _tag{tag}
	, _level{level}
{}

void Log::Verbose(const std::string& format, ...) const {
	if (_level <= LogLevel::Verbose) {
		va_list args;
		va_start(args, format);
		__android_log_vprint(ANDROID_LOG_VERBOSE, _tag.c_str(), format.c_str(), args);
		va_end(args);
	}
}

void Log::Debug(const std::string& format, ...) const {
	if (_level <= LogLevel::Debug) {
		va_list args;
		va_start(args, format);
		__android_log_vprint(ANDROID_LOG_DEBUG, _tag.c_str(), format.c_str(), args);
		va_end(args);
	}
}

void Log::Info(const std::string& format, ...) const {
	if (_level <= LogLevel::Info) {
		va_list args;
		va_start(args, format);
		__android_log_vprint(ANDROID_LOG_INFO, _tag.c_str(), format.c_str(), args);
		va_end(args);
	}
}

void Log::Warning(const std::string& format, ...) const {
	if (_level <= LogLevel::Warning) {
		va_list args;
		va_start(args, format);
		__android_log_vprint(ANDROID_LOG_WARN, _tag.c_str(), format.c_str(), args);
		va_end(args);
	}
}

void Log::Error(const std::string& format, ...) const {
	if (_level <= LogLevel::Error) {
		va_list args;
		va_start(args, format);
		__android_log_vprint(ANDROID_LOG_ERROR, _tag.c_str(), format.c_str(), args);
		va_end(args);
	}
}

} /* namespace Core */