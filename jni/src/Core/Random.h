#ifndef CORE_RANDOM_H
#define CORE_RANDOM_H

#include <chrono>
#include <random>

namespace Core {

class Random {
public:
	Random() = delete;
	Random(const Random&) = delete;
	Random(Random&&) = delete;

	Random& operator=(const Random&) = delete;
	Random&& operator=(Random&&) = delete;

	~Random() = delete;

	static int NextInt(int min, int max);
	static float NextFloat(float min, float max);
	static double NextDouble(double min, double max);

private:
	typedef std::default_random_engine RandomEngine;
	typedef std::uniform_int_distribution<int> IntDistribution;
	typedef std::uniform_real_distribution<float> FloatDistribution;
	typedef std::uniform_real_distribution<double> DoubleDistribution;

	static RandomEngine generator;
	static IntDistribution intDistribution;
	static FloatDistribution floatDistribution;
	static DoubleDistribution doubleDistribution;
};

} /* namespace Core */

#endif /* CORE_RANDOM_H */