#ifndef CORE_ASSERT_H
#define CORE_ASSERT_H

#ifdef NDEBUG
#	define Assert(ignore) ((void)0)
#else
#	include <android/log.h>
#	ifndef LOG_TAG
#		define LOG_TAG "Assert"
#	endif 
#	if __ISO_C_VISIBLE >= 1999
#		define Assert(e) ((e) ? (void)0 : __android_log_assert(0, LOG_TAG, "Assertion failed: %s, file %s, line %d, function %s", #e, __FILE__, __LINE__, __func__))
#	else
#		define Assert(e) ((e) ? (void)0 : __android_log_assert(0, LOG_TAG, "Assertion failed: %s, file %s, line %d", #e, __FILE__, __LINE__))
#	endif
#endif

#endif /* CORE_ASSERT_H */