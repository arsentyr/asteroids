#include "Core.h"

#ifndef LOG_TAG
#	define LOG_TAG "Core"
#endif

namespace Core {
#ifdef NDEBUG
	Log log(LOG_TAG, LogLevel::Error);
#else
	Log log(LOG_TAG, LogLevel::Verbose);
#endif
	Timer timer;
	Graphics graphics;
} /* namespace Core */