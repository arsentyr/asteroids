#include "Random.h"

#include "Assert.h" 

namespace Core {

Random::RandomEngine Random::generator{static_cast<unsigned>(std::chrono::system_clock::now().time_since_epoch().count())};
Random::IntDistribution Random::intDistribution;
Random::FloatDistribution Random::floatDistribution;
Random::DoubleDistribution Random::doubleDistribution;

int Random::NextInt(int min, int max) {
	Assert(min <= max);
	return intDistribution(generator, IntDistribution::param_type{min, max});
}

float Random::NextFloat(float min, float max) {
	Assert(min <= max);
	return floatDistribution(generator, FloatDistribution::param_type{min, max});
}

double Random::NextDouble(double min, double max) {
	Assert(min <= max);
	return doubleDistribution(generator, DoubleDistribution::param_type{min, max});
}

} /* namespace Core */