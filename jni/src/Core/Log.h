#ifndef CORE_LOG_H
#define CORE_LOG_H

#include <string>

namespace Core {

enum class LogLevel {
	Verbose = 0,
	Debug,
	Info,
	Warning,
	Error
};

class Log {
public:
	Log(const std::string& tag = std::string(), LogLevel level = LogLevel::Verbose);

	void Verbose(const std::string& format, ...) const;
	void Debug(const std::string& format, ...) const;
	void Info(const std::string& format, ...) const;
	void Warning(const std::string& format, ...) const;
	void Error(const std::string& format, ...) const;

private:
	const std::string _tag;
	const LogLevel _level;
};

extern Log log;

} /* namespace Core */

#endif /* CORE_LOG_H */