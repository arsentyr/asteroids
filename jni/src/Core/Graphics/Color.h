#ifndef CORE_COLOR_H
#define CORE_COLOR_H

namespace Core {

struct Color {
	static const Color Black;
	static const Color White;
	static const Color Red;
	static const Color Green;
	static const Color Blue;

	unsigned char red;
	unsigned char green;
	unsigned char blue;
	unsigned char alpha;

	constexpr Color(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha = 255)
		: red{red}
		, green{green}
		, blue{blue}
		, alpha{alpha}
	{}

	constexpr Color() : Color{255, 255, 255, 255} {}
};

} /* namespace Core */

#endif /* CORE_COLOR_H */