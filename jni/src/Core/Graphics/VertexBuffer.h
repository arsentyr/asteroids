#ifndef CORE_VERTEX_BUFFER_H
#define CORE_VERTEX_BUFFER_H

#include <vector>
#include <initializer_list>
#include "Vertex.h"

namespace Core {

class VertexBuffer {
public:
	VertexBuffer(std::size_t capacity = 0);

	VertexBuffer(std::initializer_list<Vertex> vertices);

	Vertex& operator[](std::size_t index);
	const Vertex& operator[](std::size_t index) const;

	std::size_t Count() const;

	void Add(const Vertex& vertex);
	void Add(Vertex&&);

private:
	std::vector<Vertex> _vertices;
};

} /* namespace Core */

#endif /* CORE_VERTEX_BUFFER_H */