#include "ShaderProgram.h"

#include "../Assert.h"
#include "../Log.h"

namespace Core {

ShaderProgram::~ShaderProgram() {
	Assert(!_isLoaded);
	if (_isLoaded) {
		Clear();
	}
}

bool ShaderProgram::LoadFromSource(const std::string& sourceVertexShader, const std::string& sourceFragmentShader) {
	Assert(!_isLoaded);
	Assert(sourceVertexShader.length() > 0 && sourceFragmentShader.length() > 0);
	if (_isLoaded) {
		log.Error("Shader program already loaded");
		return false;
	}

	// load vertex shader
	_vertexShader = LoadShaderFromSource(GL_VERTEX_SHADER, sourceVertexShader.c_str());
	Assert(_vertexShader != 0);
	if (glIsShader(_vertexShader) != GL_TRUE) {
		return false;
	}

	// load fragment shader
	_fragmentShader = LoadShaderFromSource(GL_FRAGMENT_SHADER, sourceFragmentShader.c_str());
	Assert(_fragmentShader != 0);
	if (glIsShader(_fragmentShader) != GL_TRUE) {
		Clear();
		return false;
	}

	// create program
	_program = glCreateProgram();
	Assert(_program != 0);
	if (glIsProgram(_program) != GL_TRUE) {
		Clear();
		log.Error("Shader program not created");
		return false;
	}

	// attach shaders to program
	glAttachShader(_program, _vertexShader);
	glAttachShader(_program, _fragmentShader);
	GLint numAttachedShaders = 0;
	glGetProgramiv(_program, GL_ATTACHED_SHADERS, &numAttachedShaders);
	Assert(numAttachedShaders == 2);
	if (numAttachedShaders < 2) {
		ProgramLog(_program, "Shaders not attached to program");
		Clear();
		return false;
	}

	_isLoaded = true;
	return true;
}

void ShaderProgram::BindAttributeLocation(unsigned int index, const std::string& name) {
	Assert(_isLoaded && !_isLinked);
	Assert(index < GetMaxVertexAttributes() && name.length() > 0);
	if (!_isLoaded || _isLinked) {
		log.Error("Shader program must be loaded and not linked");
		return;
	}

	glBindAttribLocation(_program, index, name.c_str());
}

bool ShaderProgram::Link() {
	Assert(_isLoaded && !_isLinked);
	if (!_isLoaded || _isLinked) {
		log.Error("Shader program must be loaded and not linked");
		return false;
	}

	// link program
	glLinkProgram(_program);
	GLint linkStatus = GL_FALSE;
	glGetProgramiv(_program, GL_LINK_STATUS, &linkStatus);
	Assert(linkStatus == GL_TRUE);
	if (linkStatus != GL_TRUE) {
		ProgramLog(_program, "Shader program not linked");
		Clear();
		return false;
	}

	_isLinked = true;
	return true;
}

int ShaderProgram::GetAttributeLocation(const std::string& name) const {
	Assert(_isLoaded && _isLinked);
	if (!_isLinked) {
		log.Error("Shader program must be loaded and linked");
		return kUnknownLocation;
	}

	GLint location = glGetAttribLocation(_program, name.c_str());
	Assert(location >= 0);
	if (location >= 0) {
		return location;
	}
	else {
		log.Error("Vertex attribute %s not found", name.c_str());
		return kUnknownLocation;
	}
}

int ShaderProgram::GetUniformLocation(const std::string& name) const {
	Assert(_isLoaded && _isLinked);
	if (!_isLinked) {
		log.Error("Shader program must be loaded and linked");
		return kUnknownLocation;
	}

	GLint location = glGetUniformLocation(_program, name.c_str());
	Assert(location >= 0);
	if (location >= 0) {
		return location;
	}
	else {
		log.Error("Uniform %s not found", name.c_str());
		return kUnknownLocation;
	}
}

void ShaderProgram::Use() const {
	Assert(_isLoaded && _isLinked && glIsProgram(_program) == GL_TRUE);
	
	if (_isLinked) {
		glUseProgram(_program);
	}
	else {
		log.Error("Shader program must be loaded and linked");
	}
}

void ShaderProgram::Delete() {
	Assert(_isLoaded);
	if (!_isLoaded) {
		log.Error("Shader program must be loaded");		
		Clear();
	}
}

bool ShaderProgram::IsLoaded() const {
	return _isLoaded;
}

bool ShaderProgram::IsLinked() const {
	return _isLinked;
}

bool ShaderProgram::IsValid() const {
	Assert(_isLoaded && _isLinked);
	if (!_isLinked) {
		log.Error("Shader program must be loaded and linked");
		return false;
	}

	// validate program
	glValidateProgram(_program);
    GLint validateStatus = GL_FALSE;
    glGetProgramiv(_program, GL_VALIDATE_STATUS, &validateStatus);
    if (validateStatus != GL_TRUE) {
    	ProgramLog(_program, "Shader program not valid");
    	return false;
    }

    return true;
}

int ShaderProgram::GetMaxVertexAttributes() {
	GLint maxVertexAttribs = 0;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &maxVertexAttribs);
	return maxVertexAttribs;
}

void ShaderProgram::Clear() {
	if (_vertexShader != 0 && glIsShader(_vertexShader) == GL_TRUE) {
		glDeleteShader(_vertexShader);
	}
	if (_fragmentShader != 0 && glIsShader(_fragmentShader) == GL_TRUE) {
		glDeleteShader(_fragmentShader);
	}
	if (_program != 0 && glIsProgram(_program) == GL_TRUE) {
		glDeleteProgram(_program);
	}
	_program = 0;
	_vertexShader = 0;
	_fragmentShader = 0;
	_isLoaded = false;
	_isLinked = false;
}

GLuint ShaderProgram::LoadShaderFromSource(GLenum type, const GLchar* source) {
	// create shader
	GLuint shader = glCreateShader(type);
	if (shader == 0) {
		log.Error("Shader not created");
		return 0;
	}

	// attach shader source
	glShaderSource(shader, 1, &source, nullptr);

	// compile shader
	glCompileShader(shader);
	GLint compileStatus = GL_FALSE;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);
	if (compileStatus != GL_TRUE) {
		ShaderLog(shader, "Shader not compiled");
		if (glIsShader(shader) == GL_TRUE) {
			glDeleteShader(shader);
		}
		return 0;
	}

	return shader;
}

void ShaderProgram::ShaderLog(GLuint shader, const std::string& message) {
	GLint infoLogLength = 0;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (infoLogLength > 0) {
		GLchar* infoLog = new GLchar[infoLogLength];
		glGetShaderInfoLog(shader, infoLogLength, nullptr, infoLog);
		log.Error("%s: %s", message.c_str(), infoLog);
		delete [] infoLog;
	}
	else {
		log.Error(message);
	}
}

void ShaderProgram::ProgramLog(GLuint program, const std::string& message) {
	GLint infoLogLength = 0;
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (infoLogLength > 0) {
		GLchar* infoLog = new GLchar[infoLogLength];
		glGetProgramInfoLog(program, infoLogLength, nullptr, infoLog);
		log.Error("%s: %s", message.c_str(), infoLog);
		delete [] infoLog;
	}
	else {
		log.Error(message);
	}
}

} /* namespace Core */