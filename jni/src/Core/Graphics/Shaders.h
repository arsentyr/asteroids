#ifndef CORE_SHADERS_H
#define CORE_SHADERS_H

#include <string>
#include <GLES2/gl2.h>

namespace Core { namespace Shaders {

#define SHADER_ATTRIBUTE_POSITION_NAME "a_position"
#define SHADER_ATTRIBUTE_COLOR_NAME "a_color"

#define SHADER_UNIFORM_MVP_MATRIX "mvp_matrix"

static const GLchar defaultVertexShader[] =
	"uniform mat4 mvp_matrix;                 \n"
	"attribute vec4 a_position;               \n"
	"attribute vec4 a_color;                  \n"
	"varying lowp vec4 v_fragment_color;      \n"
	"void main() {                            \n"
	"  gl_Position = mvp_matrix * a_position; \n"
	"  v_fragment_color = a_color;            \n"
	"}                                        \n";

static const GLchar defaultFragmentShader[] =
	"varying lowp vec4 v_fragment_color; \n"
    "void main() {                       \n"
    "  gl_FragColor = v_fragment_color;  \n"
    "}                                   \n";

} /* namespace Shaders */ } /* namespace Core */

#endif /* CORE_SHADERS_H */