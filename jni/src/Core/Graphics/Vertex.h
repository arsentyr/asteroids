#ifndef CORE_VERTEX_H
#define CORE_VERTEX_H

#include "Color.h"

namespace Core {

struct Vertex {
	float x;
	float y;
	float z;
	Color color;

	explicit constexpr Vertex(float x = 0, float y = 0, float z = 0, Color color = Color{})
		: x{x}
		, y{y}
		, z{z}
		, color{color}
	{}
};

} /* namespace Core */

#endif /* CORE_VERTEX_H */