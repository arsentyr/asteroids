#ifndef CORE_SHADER_PROGRAM_H
#define CORE_SHADER_PROGRAM_H

#include <string>
#include <GLES2/gl2.h>

namespace Core {

class ShaderProgram {
public:
	enum ErrorCode : int {
		kUnknownLocation = -1
	};

public:
	ShaderProgram() = default;
	
	ShaderProgram(const ShaderProgram&) = delete;
	ShaderProgram(ShaderProgram&&) = delete;
	
	ShaderProgram& operator=(const ShaderProgram&) = delete;
	ShaderProgram& operator=(ShaderProgram&&) = delete;
	
	~ShaderProgram();

	bool LoadFromSource(const std::string& sourceVertexShader, const std::string& sourceFragmentShader);

	void BindAttributeLocation(unsigned int index, const std::string& name);

	bool Link();

	int GetAttributeLocation(const std::string& name) const;

	int GetUniformLocation(const std::string& name) const;

	void Use() const;

	void Delete();
	
	bool IsLoaded() const;
	
	bool IsLinked() const;
	
	bool IsValid() const;

	static int GetMaxVertexAttributes();

private:
	void Clear();

	static GLuint LoadShaderFromSource(GLenum type, const GLchar* source);
	
	static void ShaderLog(GLuint shader, const std::string& message);
	static void ProgramLog(GLuint program, const std::string& message);

private:
	bool _isLoaded = false;
	bool _isLinked = false;
	GLuint _program = 0;
	GLuint _vertexShader = 0;
	GLuint _fragmentShader = 0;
};

} /* namespace Core */

#endif /* CORE_SHADER_PROGRAM_H */