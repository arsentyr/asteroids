#ifndef CORE_MATRIX_STACK_H
#define CORE_MATRIX_STACK_H

#include <vector>
#include "../Math/Matrix4.h"

namespace Core {

class MatrixStack {
public:
	explicit MatrixStack(std::size_t capacity, const Matrix4& top = Matrix4::Zero);

	void PushMatrix();

	void PopMatrix();

	const Matrix4& GetTopMatrix() const;

	std::size_t Count() const;

	void LoadIdentity();

	void LoadMatrix(const Matrix4& mat);

	void MultMatrix(const Matrix4& mat);

	void Scale(float scaleX, float scaleY, float scaleZ);

	void RotateX(float angle);

	void RotateY(float angle);

	void RotateZ(float angle);

	void Translate(float deltaX, float deltaY, float deltaZ);

private:
	std::vector<Matrix4> _stack;
};

} /* namespace Core */

#endif /* CORE_MATRIX_STACK_H */