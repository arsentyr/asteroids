#include "Graphics.h"

#include "Shaders.h"
#include "../Assert.h"
#include "../Log.h"
#include "../Math/Math.h"
#include "../Math/Vector2.h"
#include "../Math/Matrix4.h"

#ifdef NDEBUG
#	define CHECK_GL_ERROR() ((void)0)
#else
#	define CHECK_GL_ERROR() for (GLenum error = glGetError(); error != GL_NO_ERROR; Assert(glGetError()))
#endif

namespace Core {

Graphics::~Graphics() {
	Assert(_state == GraphicsState::Default);
	if (_state != GraphicsState::Default) {
		Clear();
	}
}

void Graphics::Initialize() {
	Assert(_state == GraphicsState::Default);
	if (_state != GraphicsState::Default) {
		log.Error("Graphics initialize failed: incorrect state=%d", _state);
		return;
	}

	Load();
	_state = GraphicsState::Initialized;
	SetLineWidth(_lineWidth);
	SetBlend(false);
	SetBlendMode(_blendMode);
	SetDepthTest(true);
}

void Graphics::Reset() {
	Assert(_state == GraphicsState::Initialized);
	if (_state == GraphicsState::Default) {
		log.Error("Graphics reset failed: incorrect state=%d", _state);
		return;
	}

	Clear();
	Load();
}

void Graphics::Release() {
	Assert(_state != GraphicsState::Default);
	if (_state == GraphicsState::Default) {
		log.Error("Graphics release failed: incorrect state=%d", _state);
		return;
	}

	Clear();
	_state = GraphicsState::Default;
}

void Graphics::SetViewport(int x, int y, int width, int height) {
	Assert(_state != GraphicsState::Default);
	if (_state == GraphicsState::Default) {
		log.Error("Graphics setting viewport failed: incorrect state=%d", _state);
		return;
	}
	Assert(width > 0 && height > 0);
	if (width <= 0 || height <= 0) {
		log.Error("Graphics setting viewport failed: width=%d and height=%d must be > 0", width,  height);
		return;
	}

	_width = width;
	_height = height;
	glViewport(x, y, _width, _height);
	CHECK_GL_ERROR();
}

void Graphics::SetOrtho(float left, float right, float bottom, float top, float znear, float zfar) {
	Assert(_state != GraphicsState::Default);
	if (_state == GraphicsState::Default) {
		log.Error("Graphics setting ortho failed: incorrect state=%d", _state);
		return;
	}

	_orthoMatrix = Matrix4{
		2 / (right - left), 0, 0, -(left + right) / (right - left),
		0, 2 / (top - bottom), 0, -(top + bottom) / (top - bottom),
		0, 0, -2 / (zfar - znear), -(zfar + znear) / (zfar - znear),
		0, 0, 0, 1
	};
}

void Graphics::SetBlend(bool isEnable) {
	Assert(_state != GraphicsState::Default);
	
	if (isEnable) {
		glEnable(GL_BLEND);
	}
	else {
		glDisable(GL_BLEND);
	}
	CHECK_GL_ERROR();
}

void Graphics::SetDepthTest(bool isEnable) {
	Assert(_state != GraphicsState::Default);
	
	if (isEnable) {
		glEnable(GL_DEPTH_TEST);
	}
	else {
		glDisable(GL_DEPTH_TEST);
	}
	CHECK_GL_ERROR();
}

void Graphics::SetBlendMode(BlendMode blendMode) {
	Assert(_state != GraphicsState::Default);

	_blendMode = blendMode;
	switch (blendMode) {
	case BlendMode::Alpha:
		glBlendEquation(GL_FUNC_ADD);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		break;

	case BlendMode::Add:
		glBlendEquation(GL_FUNC_ADD);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE);
		break;

	case BlendMode::Multiply:
		glBlendEquation(GL_FUNC_ADD);
		glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
		break;
	}
	CHECK_GL_ERROR();
}

BlendMode Graphics::GetBlendMode() const {
	Assert(_state != GraphicsState::Default);

	return _blendMode;
}

void Graphics::SetLineWidth(float lineWidth) {
	Assert(lineWidth > 0);
	Assert(_state != GraphicsState::Default);

	glLineWidth(lineWidth);
	_lineWidth = lineWidth;
}

float Graphics::GetLineWidth() const {
	return _lineWidth;
}

int Graphics::GetWidth() const {
	return _width;
}

int Graphics::GetHeight() const {
	return _height;
}

void Graphics::BeginFrame() {
	Assert(_state == GraphicsState::Initialized);
	if (_state != GraphicsState::Initialized) {
		log.Error("Graphics beginning frame failed: incorrect state=%d", _state);
		return;
	}
	CHECK_GL_ERROR();

	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT);

	Assert(_modelViewMatrixStack.Count() == 1);
	_modelViewMatrixStack.LoadMatrix(_orthoMatrix);

	_defaultShader.program.Use();
	Assert(_defaultShader.program.IsValid());

	CHECK_GL_ERROR();
	_state = GraphicsState::Framed;
}

void Graphics::EndFrame() {
	Assert(_state == GraphicsState::Framed);
	if (_state != GraphicsState::Framed) {
		log.Error("Graphics ending frame failed: incorrect state=%d", _state);
		return;
	}
	CHECK_GL_ERROR();

	glFlush();

	CHECK_GL_ERROR();
	_state = GraphicsState::Initialized;
}

void Graphics::Draw(const VertexBuffer& buffer, DrawMode mode) const {
	Draw(buffer, 0, buffer.Count(), mode);
}

void Graphics::Draw(const VertexBuffer& buffer, std::size_t offset, std::size_t count, DrawMode mode) const {
	Assert(_state == GraphicsState::Framed);
	Assert(count > 0 && (offset + count) <= buffer.Count());

	glVertexAttribPointer(kPositionIndex, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), &buffer[offset].x);
	glEnableVertexAttribArray(kPositionIndex);
	glVertexAttribPointer(kColorIndex, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex), &buffer[offset].color);
	glEnableVertexAttribArray(kColorIndex);
	glUniformMatrix4fv(_defaultShader.mvpUniformLocation, 1, GL_FALSE, &_modelViewMatrixStack.GetTopMatrix().x0);
	glDrawArrays(static_cast<GLenum>(mode), 0, count);
	glDisableVertexAttribArray(kColorIndex);
	glDisableVertexAttribArray(kPositionIndex);

	CHECK_GL_ERROR();
}

void Graphics::PushMatrix() {
	Assert(_state == GraphicsState::Framed);
	_modelViewMatrixStack.PushMatrix();
}

void Graphics::PopMatrix() {
	Assert(_state == GraphicsState::Framed);
	_modelViewMatrixStack.PopMatrix();
}

void Graphics::Scale(float scale) {
	Scale(scale, scale);
}

void Graphics::Scale(const Vector2& scale) {
	Scale(scale.x, scale.y);
}

void Graphics::Scale(float scaleX, float scaleY) {
	Assert(_state == GraphicsState::Framed);
	_modelViewMatrixStack.Scale(scaleX, scaleY, 1);
}

void Graphics::Rotate(float angle) {
	Assert(_state == GraphicsState::Framed);
	_modelViewMatrixStack.RotateZ(DegToRad(angle));
}

void Graphics::Translate(float delta) {
	Translate(delta, delta);
}

void Graphics::Translate(const Vector2& delta) {
	Translate(delta.x, delta.y);
}

void Graphics::Translate(float deltaX, float deltaY) {
	Assert(_state == GraphicsState::Framed);
	_modelViewMatrixStack.Translate(deltaX, deltaY, 0);
}

void Graphics::Load() {
	LoadShaders();
	log.Debug("Graphics resources are loaded");
}

void Graphics::Clear() {
	DeleteShaders();
	log.Debug("Graphics resources have been cleared");
}

void Graphics::LoadShaders() {
	_defaultShader.program.LoadFromSource(Shaders::defaultVertexShader, Shaders::defaultFragmentShader);
	Assert(_defaultShader.program.IsLoaded());
	
	_defaultShader.program.BindAttributeLocation(kPositionIndex, SHADER_ATTRIBUTE_POSITION_NAME);
	_defaultShader.program.BindAttributeLocation(kColorIndex, SHADER_ATTRIBUTE_COLOR_NAME);
	_defaultShader.program.Link();
	Assert(_defaultShader.program.IsLinked());
	Assert(_defaultShader.program.GetAttributeLocation(SHADER_ATTRIBUTE_POSITION_NAME) == kPositionIndex);
	Assert(_defaultShader.program.GetAttributeLocation(SHADER_ATTRIBUTE_COLOR_NAME) == kColorIndex);

	_defaultShader.mvpUniformLocation = _defaultShader.program.GetUniformLocation(SHADER_UNIFORM_MVP_MATRIX);
	Assert(_defaultShader.mvpUniformLocation != ShaderProgram::kUnknownLocation);
	CHECK_GL_ERROR();
}

void Graphics::DeleteShaders() {
	_defaultShader.program.Delete();
	_defaultShader.mvpUniformLocation = ShaderProgram::kUnknownLocation;
	CHECK_GL_ERROR();
}

} /* namespace Core */