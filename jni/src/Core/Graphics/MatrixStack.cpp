#include "MatrixStack.h"

#include "../Assert.h"
#include "../Log.h"
#include "../Math/Math.h"

namespace Core {

MatrixStack::MatrixStack(std::size_t capacity, const Matrix4& top) {
	Assert(capacity > 0);
	_stack.reserve(capacity);
	_stack.push_back(top);
}

void MatrixStack::PushMatrix() {
	const auto top = _stack.back();
	_stack.push_back(top);
}

void MatrixStack::PopMatrix() {
	Assert(Count() > 1);
	if (_stack.size() > 1) {
		_stack.pop_back();
	}
	//TODO
	//else {
		//log.Error("Matrix stack underflow");
		//throw std::runtime_error("Matrix stack underflow");
	//}
}

const Matrix4& MatrixStack::GetTopMatrix() const {
	return _stack.back();
}

std::size_t MatrixStack::Count() const {
	return _stack.size();
}

void MatrixStack::LoadIdentity() {
	_stack.back() = Matrix4::Identity;
}

void MatrixStack::LoadMatrix(const Matrix4& mat) {
	_stack.back() = mat;
}

void MatrixStack::MultMatrix(const Matrix4& mat) {
	const auto top = _stack.back();
	_stack.back() = mat * top;
}

void MatrixStack::Scale(float scaleX, float scaleY, float scaleZ) {
	MultMatrix(Matrix4{
		scaleX, 0,      0,      0,
		0,      scaleY, 0,      0,
		0,      0,      scaleZ, 0,
		0,      0,      0,      1
	});
}

void MatrixStack::RotateX(float angle) {
	const auto sine = Sin(angle);
	const auto cosine = Cos(angle);
	MultMatrix(Matrix4{
		1, 0,      0,      0,
		0, cosine, sine,   0,
		0, -sine,  cosine, 0,
		0, 0,      0,      1
	});
}

void MatrixStack::RotateY(float angle) {
	const auto sine = Sin(angle);
	const auto cosine = Cos(angle);
	MultMatrix(Matrix4{
		cosine, 0, -sine,  0,
		0,      1, 0,      0,
		sine,   0, cosine, 0,
		0,      0, 0,      1
	});
}

void MatrixStack::RotateZ(float angle) {
	const auto sine = Sin(angle);
	const auto cosine = Cos(angle);
	MultMatrix(Matrix4{
		cosine, sine,   0, 0,
		-sine,  cosine, 0, 0,
		0,      0,      1, 0,
		0,      0,      0, 1
	});
}

void MatrixStack::Translate(float deltaX, float deltaY, float deltaZ) {
	MultMatrix(Matrix4{
		1,      0,      0,      0,
		0,      1,      0,      0,
		0,      0,      1,      0,
		deltaX, deltaY, deltaZ, 1
	});
}

} /* namespace Core */