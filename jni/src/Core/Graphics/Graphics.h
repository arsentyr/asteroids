#ifndef CORE_GRAPHICS_H
#define CORE_GRAPHICS_H

#include <string>
#include <GLES2/gl2.h>
#include "MatrixStack.h"
#include "VertexBuffer.h"
#include "ShaderProgram.h"
#include "../Math/Vector2.h"
#include "../Math/Matrix4.h"

namespace Core {

enum class DrawMode : GLenum {
	Points = GL_POINTS,
	Lines = GL_LINES,
	LineStrip = GL_LINE_STRIP,
	LineLoop = GL_LINE_LOOP,
	Triangles = GL_TRIANGLES,
	TriangleStrip = GL_TRIANGLE_STRIP,
	TriangleFan = GL_TRIANGLE_FAN
};

enum class BlendMode {
	Alpha,
	Add,
	Multiply
};

class Graphics {
public:
	Graphics() = default;

	Graphics(const Graphics&) = delete;
	Graphics(Graphics&&) = delete;
	
	Graphics& operator=(const Graphics&) = delete;
	Graphics& operator=(Graphics&&) = delete;
	
	~Graphics();

	void Initialize();
	void Reset();
	void Release();

	void SetViewport(int x, int y, int width, int height);

	void SetOrtho(float left, float right, float bottom, float top, float znear, float zfar);

	void SetBlend(bool isEnable);
	void SetDepthTest(bool isEnable);

	void SetBlendMode(BlendMode blendMode);
	BlendMode GetBlendMode() const;

	void SetLineWidth(float lineWidth);
	float GetLineWidth() const;

	int GetWidth() const;
	int GetHeight() const;

	void BeginFrame();
	void EndFrame();

	void Draw(const VertexBuffer& buffer, DrawMode mode) const;

	void Draw(const VertexBuffer& buffer, std::size_t offset, std::size_t count, DrawMode mode) const;

	void PushMatrix();
	void PopMatrix();

	void Scale(float scale);
	void Scale(const Vector2& scale);
	void Scale(float scaleX, float scaleY);

	void Rotate(float angle);

	void Translate(float delta);
	void Translate(const Vector2& delta);
	void Translate(float deltaX, float deltaY);

private:
	void Load();
	void Clear();

	void LoadShaders();
	void DeleteShaders();

private:
	enum class GraphicsState {
		Default,
		Initialized,
		Framed
	};

private:
	int _width = 0;
	int _height = 0;
	GraphicsState _state = GraphicsState::Default;

	float _lineWidth = 1;
	BlendMode _blendMode = BlendMode::Alpha;

	enum {
		kPositionIndex = 0,
		kColorIndex
	};

	struct {
		ShaderProgram program;
		int mvpUniformLocation = ShaderProgram::kUnknownLocation;
	} _defaultShader;

	Matrix4 _orthoMatrix = Matrix4::Identity;
	MatrixStack _modelViewMatrixStack{5};
};

extern Graphics graphics;

} /* namespace Core */

#endif /* CORE_GRAPHICS_H */