#include "VertexBuffer.h"

#include "../Assert.h"

namespace Core {

VertexBuffer::VertexBuffer(std::size_t capacity)
	: _vertices{capacity}
{}

VertexBuffer::VertexBuffer(std::initializer_list<Vertex> vertices)
	: _vertices{vertices}
{}

Vertex& VertexBuffer::operator[](std::size_t index) {
	Assert(index >= 0 && index < Count());
	return _vertices[index];
}

const Vertex& VertexBuffer::operator[](std::size_t index) const {
	Assert(index >= 0 && index < Count());
	return _vertices[index];
}

std::size_t VertexBuffer::Count() const {
	return _vertices.size();
}

void VertexBuffer::Add(const Vertex& vertex) {
	_vertices.push_back(vertex);
}

void VertexBuffer::Add(Vertex&& vertex) {
	_vertices.push_back(vertex);
}

} /* namespace Core */