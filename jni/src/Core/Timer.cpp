#include "Timer.h"

#include "Assert.h"

using std::chrono::steady_clock;
using std::chrono::duration;

namespace Core {

void Timer::Start() {
	Assert(!_started);

	_time = steady_clock::now();
	_started = true;
}

void Timer::Stop() {
	Assert(_started);

	_started = false;
}

float Timer::GetDeltaTime() {
	Assert(_started);
	if (!_started) {
		return 0;
	}

	const auto now = steady_clock::now();
	const auto deltaTime = duration<float>(now - _time).count();
	_time = now;
	return deltaTime;
}

} /* namespace Core */
