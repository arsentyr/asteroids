#ifndef CORE_TIMER_H
#define CORE_TIMER_H

#include <chrono>

namespace Core {

class Timer {
public:
	void Start();

	void Stop();
	
	float GetDeltaTime();

private:
	bool _started = false;
	std::chrono::time_point<std::chrono::steady_clock> _time;
};

extern Timer timer;

} /* namespace Core */

#endif /* CORE_TIMER_H */
