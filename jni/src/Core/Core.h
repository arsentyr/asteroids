#ifndef CORE_H
#define CORE_H

#include "Assert.h"
#include "Log.h"
#include "Timer.h"
#include "Random.h"
#include "Math/Math.h"
#include "Math/Vector2.h"
#include "Math/Matrix4.h"
#include "Graphics/Color.h"
#include "Graphics/Vertex.h"
#include "Graphics/VertexBuffer.h"
#include "Graphics/Graphics.h"
#include "Tween/Tween.h"

using Random = Core::Random;
using Vector2 = Core::Vector2;
using Matrix4 = Core::Matrix4;
using Color = Core::Color;
using Vertex = Core::Vertex;
using VertexBuffer = Core::VertexBuffer;
using DrawMode = Core::DrawMode;
using BlendMode = Core::BlendMode;
using TweenEasing = Core::TweenEasing;
using TweenMode = Core::TweenMode;

typedef Core::Tween<float> Tweenf;
typedef Core::Tween<Vector2> Tweenv;

#endif /* CORE_H */