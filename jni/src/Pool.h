#ifndef ASTEROIDS_POOL_H
#define ASTEROIDS_POOL_H

#include <memory>

#define PRINT_POOL()																			\
do {																							\
	Core::log.Debug("first=%d, last=%d", _first, _last);										\
	for (auto curr = _first; curr != _end; curr = _pool[curr]->next) {							\
		Core::log.Debug("id=%d, prev=%d, next=%d", curr, _pool[curr]->prev, _pool[curr]->next);	\
	}																							\
	Core::log.Debug("head=%d, tail=%d", _head, _tail);											\
	for (auto curr = _head; curr != _end; curr = _pool[curr]->next) {							\
		Core::log.Debug("id=%d, prev=%d, next=%d", curr, _pool[curr]->prev, _pool[curr]->next);	\
	}																							\
} while (false)																					\

#ifdef NDEBUG
#	define CHECK_POOL() ((void)0)
#	define CHECK_IN_USE_POOL(ignore) ((void)0)
#else
#	define CHECK_POOL() 															\
	do {																			\
		std::size_t count = 0;														\
		for (auto curr = _first; curr != _end; curr = _pool[curr]->next, ++count);	\
		for (auto curr = _head; curr != _end; curr = _pool[curr]->next, ++count);	\
		if (count != _pool.size()) {												\
			PRINT_POOL();															\
		}																			\
		Assert(count == _pool.size());                                              \
		count = _pool.size();												        \
		for (auto curr = _last; curr != _end; curr = _pool[curr]->prev, --count);	\
		for (auto curr = _tail; curr != _end; curr = _pool[curr]->prev, --count);	\
		if (count != 0) {															\
			PRINT_POOL();															\
		}																			\
		Assert(count == 0);															\
	} while(false)
#
#	define CHECK_IN_USE_POOL(id)				\
	do {										\
		int curr = _first;						\
		while (curr != id && curr != _end) {	\
			curr = _pool[curr]->next;			\
		}										\
		if (curr == _end) {						\
			PRINT_POOL();						\
		}										\
		Assert(curr != _end);					\
	} while	(false)
#endif

namespace Asteroids {

class Poolable {
template <typename T> friend class Pool;
private:
	int id;
	int prev;
	int next;
};

template <typename T>
class Pool {
public:
	Pool() = delete;
	Pool(const Pool&) = delete;
	Pool(Pool&&) = delete;

	Pool& operator=(const Pool&) = delete;
	Pool& operator=(Pool&&) = delete;

	~Pool() = default;

	Pool(int capacity) {
		Assert(capacity > 0);

		if (capacity > 0) {
			_pool.reserve(capacity);
			for (auto prev = _end, curr = 0, next = 1;
					curr < capacity;
					prev = curr, curr = next, ++next) {
				_pool.push_back(std::unique_ptr<T>{new T});
				_pool.back()->prev = prev;
				_pool.back()->id = curr;
				_pool.back()->next = next;
			}
			_pool.back()->next = _end;
			_head = 0;
			_tail = _pool.size() - 1;
		}

		CHECK_POOL();
	}
	
	T& Take() {
		auto id = DequeueUnused();
		if (id == _end) {
			_pool.push_back(std::unique_ptr<T>{new T});
			id = _pool.size() - 1;
			_pool.back()->id = id;
		}
		AddUsed(id);
		CHECK_POOL();
		return *_pool[id];
	}

	void Delete(const T& object) {
		const auto id = object.id;
		Assert(id >= 0 && id < _pool.size());
		Assert(&object == &(*_pool[id]));
		if (id < 0 || id >= _pool.size()
				|| &object != &(*_pool[id])) {
			return;
		}

		RemoveUsed(id);
		EnqueueUnused(id);
		CHECK_POOL();
	}

private:
	int DequeueUnused() {
		if (_head == _end) {
			return _end;
		}
		else if (_head == _tail) {
			const auto id = _head;
			_head = _end;
			_tail = _end;
			return id;
		}
		else {
			const auto id = _head;
			_head = _pool[id]->next;
			_pool[_head]->prev = _end;
			return id;
		} 
	}

	void EnqueueUnused(int id) {
		if (_head == _end) {
			_head = id;
			_tail = id;
			_pool[id]->prev = _end;
			_pool[id]->next = _end;
		}
		else {
			_pool[_tail]->next = id;
			_pool[id]->prev = _tail;
			_pool[id]->next = _end;
			_tail = id;
		}
	}

	void RemoveUsed(int id) {
		Assert(_first != _end);
		if (_first == _end) {
			return;
		}
		CHECK_IN_USE_POOL(id);

		if (_first == _last) {
			Assert(_first == id);
			if (_first == id) {
				_first = _end;
				_last = _end;
			}
		}
		else if (_first == id) {
			_first = _pool[id]->next;
			_pool[_first]->prev = _end;
		}
		else if (_last == id) {
			_last = _pool[id]->prev;
			_pool[_last]->next = _end;
		}
		else {
			const auto prev = _pool[id]->prev;
			const auto next = _pool[id]->next;
			Assert(_pool[prev]->next == id && _pool[next]->prev == id);
			if (_pool[prev]->next == id) {
				_pool[prev]->next = next;
				_pool[next]->prev = prev;
			}
		}
	}

	void AddUsed(int id) {
		if (_first == _end) {
			_first = id;
			_last = id;
			_pool[id]->prev = _end;
			_pool[id]->next = _end;
		}
		else {
			_pool[_last]->next = id;
			_pool[id]->prev = _last;
			_pool[id]->next = _end;
			_last = id;
		}
	}

private:
	typedef std::vector<std::unique_ptr<T>> Container;

private:
	Container _pool;
	static constexpr int _end = -1;	// fake end of list objects
	int _head = _end;				// head of unused objects
	int _tail = _end;				// tail of unused objects
	int _first = _end;				// first of used objects
	int _last = _end;				// last of used objects

public:
	class ForwardIterator {
	friend class Pool;
	public:
		ForwardIterator() = delete;
		ForwardIterator(const ForwardIterator&) = default;
		ForwardIterator(ForwardIterator&&) = default;

		ForwardIterator& operator=(const ForwardIterator&) = delete;
		ForwardIterator& operator=(ForwardIterator&&) = delete;

		bool operator!=(const ForwardIterator& iterator) const {
			return _curr != iterator._curr;
		}

		ForwardIterator& operator++() {
			_curr = _next;
			_next = (_curr != _end) ? _pool[_curr]->next : _end;
			return *this;
		}

		T& operator*() {
			Assert(_pool[_curr]->next == _next);
			return *_pool[_curr];
		}

	private:
		ForwardIterator(const Container& pool, int position)
			: _pool(pool)
			, _curr{position}
			, _next{(position != _end) ? _pool[position]->next : _end}
		{}

	private:
		const Container& _pool;
		int _curr;
		int _next;
	};

public:
	ForwardIterator begin() const {
		return {_pool, _first};
	}

	const ForwardIterator end() const {
		return {_pool, _end};
	}
};

}

#endif /* ASTEROIDS_POOL_H */